module.exports = function(app, express,passport) {
  var purchaseOrderController = require('controllers/purchaseOrderController.js');
  var router = express.Router();
  var multer = require('multer');
  
  var storage = multer.diskStorage({
    // destination
    destination: function (req, file, cb) {
      cb(null, __dirname + '/../public/upload/')
    },
    filename: function (req, file, cb) {
      var file_extension = file.originalname.split('.').pop();
      cb(null, new Date().getTime()+"."+file_extension );
    }
  });
  
  var upload = multer({
    storage: storage
  })

  var type = upload.single('files');

  router.post('/create', type, passport.authenticate('bearer', { session: false }), purchaseOrderController.create );

  router.post('/update', type, purchaseOrderController.update );

  router.get('/get', purchaseOrderController.get );
  
  router.get('/', passport.authenticate('bearer', { session: false }), purchaseOrderController.getAll );

  
  router.post('/email', purchaseOrderController.email );  


  app.use('/purchase-orders', router);

}