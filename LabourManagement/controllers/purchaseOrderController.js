var purchaseOrderService = require('services/purchaseOrderService');
var constantObj = require("config/constant.js");
var pdfService = require('services/pdfService');
var pdfController = require('controllers/pdfController');
var env = require("env.js");
var configuration = require('configuration.js').get(env.environment);
var Q = require('q');
/*________________________________________________________________________________*
@Date:  06 Jan, 2018*
@Method : create new purchase order*
Created By: smartData Enterprises Ltd*
Modified On: -*
@Purpose: Function to create new purchase order
Parameters : payload
.__________________________________________________________________________________*/
exports.create = function(req, res) {
	try {

		req.body = req.body.payload ? JSON.parse(req.body.payload) : req.body;

		req.body.company_id = res.req.user.user.company_id;
		req.body.user_id = res.req.user.user._id;

		if(req.file && req.file.filename){
			req.body.attachment = '/' + 'upload/' + req.file.filename;
		}

		purchaseOrderService.create( req.body )
		.then( (data) => {
			res.json(data);
		})
		.catch( (err) => {
			res.json(err);
		});

	} catch (e) {
		res.json(e);
	}
}
//END

exports.update = (req, res) => {
	try{
		let obj = {};

		obj = req.body.payload ? JSON.parse(req.body.payload) : req.body;

		if( req.body.payload ){
			
			obj = JSON.parse(req.body.payload);

			if(req.file && req.file.filename){
				obj.attachment = '/' + 'upload/' + req.file.filename;
			}

		}else{
			obj = req.body;
		}

		purchaseOrderService.update( obj )
			.then((result)=>{				
				res.json(result);
			})
			.catch((error)=>{				
				res.json(error);
			})
	}
	catch(e){
		res.json(e);
	}
}

/**
* get purchase order template
*/
exports.get = (req, res) => {
	try{
		purchaseOrderService.get(req.query)
			.then((result)=>{				
				res.json(result);
			})
			.catch((error)=>{				
				res.json(error);
			})
	}
	catch(e){
		res.json(e);
	}
}

exports.email = (req, res) => {
	try{
		exports.sendEmail(req.body)
			.then( data => {
				res.json(data);
			})
			.catch( error => {
				res.status(400).json(error);
			});	
	}
	catch(error){
		res.status(400).json(error);
	}
}


exports.sendEmail = payload => {
	
	var deferred = Q.defer();

	getPdf( payload.query )
		.then( data => {

			let obj = payload.data;
			obj.filename = data.filename;				
			obj.logo = configuration.serverUrl + "/images/"+"logo.png";
			purchaseOrderService.email(obj)
				.then((result) => {					
					deferred.resolve( result );						
				})
				.catch((error)=>{					
					deferred.reject(error);						
				});
		})
		.catch((error) => {
			deferred.reject(error);				
		});

	return deferred.promise;	
}




/*
* this function creates pdf of the purchase order and returns file name...
*/
const getPdf = query => {

	return new Promise( (resolve, reject) => {

		pdfController.getPoHtml(query)
			.then( (html) => {
				
				// we got the html, call generate pdf service now...

				var options = { format: 'Letter' };

				pdfService.generatePDF(html, options)
					.then((data) => {						
						var filename = data.filename;						
						resolve( {filename: data.filename});						
					})
					.catch((error) => {						
						reject(error);
					});

			})
			.catch( (error) => {				
				reject(error);
			})

	});
}


/*________________________________________________________________________________*
@Date:  10 February 2018*
@Method : getAll
Created By: smartData Enterprises Ltd*
Modified On: -*
@Purpose: Function to get purchase orders
Parameters : filters
.__________________________________________________________________________________*/
exports.getAll = function(req, res) {
	try {

		var obj = req.query;

		let query = JSON.parse(req.query.query);

		var searchQryNumber = req.query.search;

		if (isNaN(searchQryNumber)) {
			var searchKeywordNumber = '';
		} else {
			var searchKeywordNumber = searchQryNumber;
		}

		var page = req.query.currentPage,
			count = parseInt(req.query.itemsPerPage);
		var searchQry = req.query.search;

		var isDeleted = req.query.isDeleted;

		let userInfo = res.req.user.user; 
		let userData = userInfo.company_id ? userInfo : userInfo.toObject();
		var company_id = userData['company_id'];
		
		var filter = [{}];

		var skipNo = (page - 1) * count;

		// check if isDeleted set to all??
		if(  ( !("search" in obj) || searchQry == '' )  && isDeleted !== 'all' ){									
			filter.push({
				isDeleted: isDeleted
			});			
		}else if( searchQry != '' && isDeleted !== 'all' ){
			filter.push({
				$or: [{
					po_number:  searchKeywordNumber					
				},
				{
					poStatus: {
						$regex: searchQry,
						$options: 'i'
					}
				}],
				$and: [{
					isDeleted: isDeleted
				}]
			});
		}else if( searchQry != '' && isDeleted == 'all' ){
			filter.push({
				$or: [
						{ po_number:  searchKeywordNumber },
						{ poStatus: { $regex: searchQry, $options: 'i' }},
						{ vendor: { $elemMatch: { display: { $regex: searchQry, $options: 'i' } } } },
						{ gtotal: { $regex: searchQry } }
					]
			});
		}

		var obj = {};		
		obj.count = count;		
		obj.sort = req.query.sort; 
		obj.skipNo = skipNo;
		obj.company_id = company_id;
		obj.filter = filter;
		obj.query = query;

		purchaseOrderService.getAll(obj).then(function(data) {			
			res.json(data);			
		}).catch(function(error) {		
			res.status(400).json(error);
		})
	} catch (error) {
		res.status(400).json(error);
	}
}

//END


