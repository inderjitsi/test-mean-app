﻿require('rootpath')();
var env = require('env');
var express = require('express');
var mongoose = require('mongoose');
var app = express();
var cors = require('cors');
var bodyParser = require('body-parser');
var expressJwt = require('express-jwt');
var configuration = require('configuration.js').get(env.environment);
var db = require('config/db')(configuration);
var config = require('config.json');
var path = require("path");
var cookieParser = require('cookie-parser');
var session = require('express-session');
var passport = require('passport');
var authentication = require('config/passport.js');
var userService = require('services/user.service');
let taskController = require('controllers/taskController.js')
logging = (arg1,arg2,data) => {
    // return;
    if(env.environment=="development"){
        console.log(arg1,arg2,data)
    }
}

// unauthenticated apis

var arrAPIs = [  
];

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

// use JWT auth to secure the api, the token can be passed in the authorization header or querystring
app.use(expressJwt({
    secret: config.secret,
    getToken: function (req) {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            return req.headers.authorization.split(' ')[1];
        } else if (req.query && req.query.token) {
            return req.query.token;
        }
        return null;
    }
}).unless({ path: arrAPIs  }));

/*
* Check if the user is authorized to access the requested end point ..
*
* Note:::: Do not change the order of middleware...
*/
app.use( function (req, res, next) {

    var apiUrl =  req.originalUrl.split("?").shift();

    if( arrAPIs.indexOf(apiUrl) == -1 ){

        var token = "";

        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            token = req.headers.authorization.split(' ')[1];
        } else if (req.query && req.query.token) {
            token = req.query.token;
        }

        try{
            userService.validateUser(token)
                .then((data)=>{
                    next();
                })
                .catch((error)=>{
                    res.status(402).json( error );
                });
        }catch(e){
            res.status(402).json( e );
        }
    }else{
        next();
    }
});

app.use(passport.initialize());
app.use(passport.session());
app.use(session({
    secret: 'keyboard-cat',
    cookie: {
        maxAge: 26280000000000
    },
    proxy: true,
    resave: true,
    saveUninitialized: true
}));
app.use(cookieParser());

var port = 80;

var server = app.listen(port, () => {
    taskController.recurable_task();
});
