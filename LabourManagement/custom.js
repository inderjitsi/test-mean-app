///////////////////////////////////////////////////////
// Custom Functions ///////////////////////////////////
///////////////////////////////////////////////////////

exports.formatDate = (date) => {
	try{

		var date = new Date(date);

		var monthNames = [
	    "January", "February", "March",
	    "April", "May", "June", "July",
	    "August", "September", "October",
	    "November", "December"
	  ];

	  var day = date.getDate();
	  var monthIndex = date.getMonth();
	  var year = date.getFullYear();

	  return day + ' ' + monthNames[monthIndex] + ' ' + year;
	}catch(e){		
		return date;
	}  
}

exports.getCompanyId = (data) => {

	let userInfo = data.req.user.user;

	if (typeof userInfo !== 'undefined'){
		let userData = userInfo && userInfo.company_id ? userInfo : userInfo.toObject();
		return company_id = data.req.user ? userData['company_id'] : '';	
	}else return false;	
}

exports.getUserId=(data)=>{
	let userInfo = data.req.user.user;	
	return user_id=data.req.user?userInfo._id:'';
}

/*
  * get formatted date and time
  */
exports.getFormattedDate = date => {     

	let d = new Date(date);

    let formatted_date = d.getFullYear() + "/" + ('0' + (d.getMonth() + 1)).slice(-2) + "/" + ('0' + d.getDate()).slice(-2) + " " + ('0' + d.getHours()).slice(-2) + ":" + ('0' + d.getMinutes()).slice(-2);

    return formatted_date;
}

exports.getMeridianTime = date => {
  
	var currentDate = new Date(date);

	var hour = currentDate.getHours();

	var meridiem = hour >= 12 ? "PM" : "AM";

	var full_hours =  ( '0' + ((hour + 11) % 12 + 1)).slice(-2);

	var full_minutes = ('0' + currentDate.getMinutes()).slice(-2);

	var full_time = full_hours + ':' + full_minutes + meridiem;

	return full_time;
}
/*
*return fullName of logged in user
*/
exports.getUserFullName=(data)=>{
	let userInfo = data.req.user.user;		
	return userInfo.firstName+' '+userInfo.lastName;
}