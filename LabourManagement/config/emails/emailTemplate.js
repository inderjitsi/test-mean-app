var nodemailer = require('nodemailer');
var fs = require('fs');
var path = require('path');
var moment = require('moment');
var objSMTP = require("./emailConstant.js");
let constants = require('../constant.js');
var rootPath = require('configuration.js').rootPath();


// create reusable transporter object using the default SMTP transport
var transporter = nodemailer.createTransport({
    host: objSMTP.host,
    port: objSMTP.port,
    secureConnection: false,
    auth: {
        user: objSMTP.username,
        pass: objSMTP.password
    }
});
  String.prototype.replaceAll = function(s, r) {
        return this.split(s).join(r)
    }
module.exports = function(type, bodyInfo, callback) {
    var sendToEmail = bodyInfo.email;
    if (sendToEmail) {
        var mailOptions = {};
        var mailOptions = {
            from: objSMTP.fromEmail, // sender address
        };


        var htmlFile;
        switch (type) {
            case 'forgotPassword':
                htmlFile = fs.readFileSync(path.join(__dirname + '/template/forgotPassword.html'), 'utf-8');
                htmlFile = htmlFile.replace('$name', bodyInfo.name).replaceAll('$email', bodyInfo.email).replace("$password", bodyInfo.emailPass).replace("$logo", bodyInfo.logo);
                mailOptions.to = sendToEmail;
                mailOptions.subject = 'Your Perdeum Password Reset';
                mailOptions.html = htmlFile;

                sendMail(mailOptions, function(emailerr, emailsuccess) {
                    if (emailerr) {
                        return callback(emailerr, null);
                    } else {
                        return callback(null, emailsuccess)
                    }
                });
                break;

            case 'registerUser':
                htmlFile = fs.readFileSync(path.join(__dirname + '/template/registerUser.html'), 'utf-8');
                htmlFile = htmlFile.replace('$name', bodyInfo.name).replace('$email', bodyInfo.email).replaceAll("$confirmationToken", bodyInfo.confirmationToken).replace("$logo", bodyInfo.logo);
                mailOptions.to = sendToEmail;
                mailOptions.subject = 'Finish your Perdeum Registration';
                mailOptions.html = htmlFile;
                sendMail(mailOptions, function(emailerr, emailsuccess) {
                    if (emailerr) {
                        return callback(emailerr, null);
                    } else {
                        return callback(null, emailsuccess)
                    }
                });
                break;

            case 'quoteRequest':
                 htmlFile = fs.readFileSync(path.join(__dirname+'/template/quoteRequest.html'),'utf-8');
                 htmlFile = htmlFile.replace('$logo',bodyInfo.logo).replaceAll('$url',bodyInfo.url).replace('$location',bodyInfo.locationName).replace('$vendorName',bodyInfo.vendorName).replace('$email',bodyInfo.email);
                 mailOptions.to = sendToEmail;
                 mailOptions.subject = "Product Bid Request";
                 mailOptions.html = htmlFile;
                 if(bodyInfo.locationEmail){
                    mailOptions.cc = bodyInfo.locationEmail;
                 }

                 sendMail(mailOptions,function(emailerr,emailsuccess){
                     if(emailerr){
                        return callback(emailerr,null);
                     }else{
                        return callback(null,emailsuccess);
                     }
                 });
                 break;

            case 'poQuote':
            bodyInfo.name = bodyInfo.name ? bodyInfo.name .charAt(0).toUpperCase() + bodyInfo.name .slice(1) : bodyInfo.name;
            bodyInfo.location = bodyInfo.location ? bodyInfo.location .charAt(0).toUpperCase() + bodyInfo.location .slice(1) : bodyInfo.location;
                htmlFile = fs.readFileSync(path.join(__dirname+'/template/poQuote.html'),'utf-8');
                htmlFile = htmlFile
                            .replace('$logo',bodyInfo.logo)
                            .replaceAll('$url',bodyInfo.url)
                            .replace('$vendor_name', bodyInfo.name )
                            .replace('$location', bodyInfo.location )
                            .replace('$email', bodyInfo.email );
                mailOptions.to = sendToEmail;
                mailOptions.subject = bodyInfo.subject;
                mailOptions.html = htmlFile;
                sendMail(mailOptions,function(emailerr,emailsuccess){
                 if(emailerr){
                    return callback(emailerr,null);
                 }else{
                    return callback(null,emailsuccess);
                 }
                });
                break;
            case 'sendPOEmail':
                html = fs.readFileSync(path.join(__dirname + '/template/purchaseOrder.html'), 'utf-8');
                html = html
                        .replace('$body', bodyInfo.body )
                        .replace('$logo', bodyInfo.logo )
                        .replace('$email', bodyInfo.email );

                mailOptions.to = sendToEmail;
                mailOptions.subject = bodyInfo.subject;
                mailOptions.html = html;
                mailOptions.attachments = [{ filename: 'PurchaseOrder.pdf', path: rootPath + '/public/pdfs/'+bodyInfo.filename, contentType: 'application/pdf' }];

                sendMail(mailOptions, function(emailerr, emailsuccess) {
                    if (emailerr) {
                        return callback(emailerr, null);
                    } else {
                        return callback(null, emailsuccess)
                    }
                });
                break;
            case 'POReceipt':
                html = fs.readFileSync(path.join(__dirname + '/template/POReceipt.html'), 'utf-8');
                html = html
                        .replace('$body', bodyInfo.body )
                        .replace('$logo', bodyInfo.logo )
                        .replace('$email', bodyInfo.email );

                mailOptions.to = sendToEmail;
                mailOptions.subject = bodyInfo.subject;
                mailOptions.html = html;
                mailOptions.attachments = [{ filename: 'PurchaseOrderReceipt.pdf', path: rootPath + '/public/pdfs/'+bodyInfo.filename, contentType: 'application/pdf' }];

                sendMail(mailOptions, function(emailerr, emailsuccess) {
                    if (emailerr) {
                        return callback(emailerr, null);
                    } else {
                        return callback(null, emailsuccess)
                    }
                });
                break;

            case 'POBill':
                html = fs.readFileSync(path.join(__dirname + '/template/POBill.html'), 'utf-8');
                html = html
                        .replace('$body', bodyInfo.body )
                        .replace('$logo', bodyInfo.logo )
                        .replace('$email', bodyInfo.email );

                mailOptions.to = sendToEmail;
                mailOptions.subject = bodyInfo.subject;
                mailOptions.html = html;
                mailOptions.attachments = [{ filename: 'PurchaseOrderBill.pdf', path: rootPath + '/public/pdfs/'+bodyInfo.filename, contentType: 'application/pdf' }];

                sendMail(mailOptions, function(emailerr, emailsuccess) {
                    if (emailerr) {
                        return callback(emailerr, null);
                    } else {
                        return callback(null, emailsuccess)
                    }
                });
                break;
            case 'POPayment':
                html = fs.readFileSync(path.join(__dirname + '/template/POPayment.html'), 'utf-8');
                html = html
                        .replace('$body', bodyInfo.body )
                        .replace('$logo', bodyInfo.logo )
                        .replace('$email', bodyInfo.email );

                mailOptions.to = sendToEmail;
                mailOptions.subject = bodyInfo.subject;
                mailOptions.html = html;
                mailOptions.attachments = [{ filename: 'PurchaseOrderPayment.pdf', path: rootPath + '/public/pdfs/'+bodyInfo.filename, contentType: 'application/pdf' }];

                sendMail(mailOptions, function(emailerr, emailsuccess) {
                    if (emailerr) {
                        return callback(emailerr, null);
                    } else {
                        return callback(null, emailsuccess)
                    }
                });
                break;
            case 'employeeRegister':
                htmlFile = fs.readFileSync(path.join(__dirname + '/template/employeeRegister.html'), 'utf-8');
                htmlFile = htmlFile.replace('$name', bodyInfo.name).replace('$email', bodyInfo.email).replaceAll("$password", bodyInfo.employeePassword).replace("$logo", bodyInfo.logo);

                mailOptions.to = sendToEmail;
                //mailOptions.subject = 'Account Registration - '+bodyInfo.companyName;
                mailOptions.subject = 'Finish your Perdeum Registration';
                mailOptions.html = htmlFile;
                sendMail(mailOptions, function(emailerr, emailsuccess) {
                    if (emailerr) {
                        return callback(emailerr, null);
                    } else {
                        return callback(null, emailsuccess)
                    }
                });
                break;
            case 'requestApproval':

                htmlFile = fs.readFileSync(path.join(__dirname + '/template/employeeRegister.html'), 'utf-8');
                htmlFile = htmlFile.replace('ZAAMIt', bodyInfo.name).replace('testing.fast411@gmail.com', bodyInfo.email).replaceAll("sdyguuisdg", bodyInfo.employeePassword).replace("http://www.javascriptkit.com/jkincludes/jklogosmall.gif", bodyInfo.logo);

                mailOptions.to = sendToEmail;
                //mailOptions.subject = 'Account Registration - '+bodyInfo.companyName;
                mailOptions.subject = 'Approve a request.';
                mailOptions.html = htmlFile;
                sendMail(mailOptions, function(emailerr, emailsuccess) {
                    if (emailerr) {
                        return callback(emailerr, null);
                    } else {
                        return callback(null, emailsuccess)
                    }
                });
                break;
            case 'publishShift':

                html = fs.readFileSync(path.join(__dirname + '/template/publishShift.html'), 'utf-8');

                html = html
                        .replace('$body', bodyInfo.body )
                        .replace('$logo', bodyInfo.logo )
                        .replace('$name', bodyInfo.name )
                        .replace('$email', bodyInfo.email );

                mailOptions.to = sendToEmail;

                mailOptions.subject = 'Shift Published';

                mailOptions.html = html;

                sendMail(mailOptions, function(emailerr, emailsuccess) {
                    if (emailerr) {
                        return callback(emailerr, null);
                    } else {
                        return callback(null, emailsuccess)
                    }
                });

                break;
            case 'timeoffApproveReject':

                html = fs.readFileSync(path.join(__dirname + '/template/timeoffApproveReject.html'), 'utf-8');

                html = html
                        .replace('$body', bodyInfo.body )
                        .replace('$logo', bodyInfo.logo )
                        .replace('$name', bodyInfo.name )
                        .replace('$email', bodyInfo.email );

                mailOptions.to = sendToEmail;

                mailOptions.subject = bodyInfo.subject;

                mailOptions.html = html;

                sendMail(mailOptions, function(emailerr, emailsuccess) {
                    if (emailerr) {
                        return callback(emailerr, null);
                    } else {
                        return callback(null, emailsuccess)
                    }
                });

                break;

            case 'timeOffRequest':

                htmlFile = fs.readFileSync(path.join(__dirname + '/template/employeeRegister.html'), 'utf-8');
                htmlFile = htmlFile.replace('$name', bodyInfo.name).replace('testing.fast411@gmail.com', bodyInfo.email).replaceAll("sdyguuisdg", bodyInfo.employeePassword).replace("http://www.javascriptkit.com/jkincludes/jklogosmall.gif", bodyInfo.logo);
                htmlFile = fs.readFileSync(path.join(__dirname + '/template/request_timeoff.html'), 'utf-8');
                htmlFile = htmlFile.replace('$employeeName', bodyInfo.employeeName).replace('$email', bodyInfo.email).
                replace("$logo", bodyInfo.logo).replace("$startDate", bodyInfo.startDate).replace("$endDate", bodyInfo.endDate)
                .replace("$reason", bodyInfo.reason);

                mailOptions.to = sendToEmail;
                //mailOptions.subject = 'Account Registration - '+bodyInfo.companyName;
                mailOptions.subject = 'A timeOff has been requested.';
                mailOptions.html = htmlFile;
                sendMail(mailOptions, function(emailerr, emailsuccess) {
                    if (emailerr) {
                        return callback(emailerr, null);
                    } else {
                        return callback(null, emailsuccess)
                    }
                });
                break;

            case 'requestCover_tradeShift':

                htmlFile = fs.readFileSync(path.join(__dirname + '/template/requestCover_tradeShift.html'), 'utf-8');
                htmlFile = htmlFile.replace('$employeeName', bodyInfo.employeeName).replace('$email', bodyInfo.email).replace("$logo", bodyInfo.logo).replace("$requesterName", bodyInfo.requesterName).replace("$message", bodyInfo.message);

                mailOptions.to = sendToEmail;
                mailOptions.subject = bodyInfo.subject;
                mailOptions.html = htmlFile;
                sendMail(mailOptions, function(emailerr, emailsuccess) {
                    if (emailerr) {
                        return callback(emailerr, null);
                    } else {
                        return callback(null, emailsuccess)
                    }
                });
                break;
            case constants.taskNotification.taskAssign:
                html = fs.readFileSync(path.join(__dirname + '/template/taskAssign.html'), 'utf-8');

                html = html
                        .replace('$body', bodyInfo.body )
                        .replace('$logo', bodyInfo.logo )
                        .replace('$name', bodyInfo.name )
                        .replace('$email', bodyInfo.email );

                mailOptions.to = sendToEmail;

                mailOptions.subject = 'Task Assigned';

                mailOptions.html = html;

                sendMail(mailOptions, function(emailerr, emailsuccess) {
                    if (emailerr) {
                        return callback(emailerr, null);
                    } else {
                        return callback(null, emailsuccess)
                    }
                });
                break;
            case 'update_request':

                htmlFile = fs.readFileSync(path.join(__dirname + '/template/update_request.html'), 'utf-8');
                htmlFile = htmlFile.replace('$employeeName', bodyInfo.employeeName).replace('$email', bodyInfo.email).
                replace("$logo", bodyInfo.logo).replace("$requesterName", bodyInfo.requesterName).
                replace("$cover_trade", bodyInfo.cover_trade).replace("$accepted_rejected", bodyInfo.accepted_rejected);

                mailOptions.to = sendToEmail;
                mailOptions.subject = bodyInfo.subject;
                mailOptions.html = htmlFile;
                sendMail(mailOptions, function(emailerr, emailsuccess) {
                    if (emailerr) {
                        return callback(emailerr, null);
                    } else {
                        return callback(null, emailsuccess)
                    }
                });
                break;
        }
    }

    function sendMail(mailOptions, cb) {
        transporter.sendMail(mailOptions, function(error, info) {
            if (error) {
                return cb(error, null);
            } else {
                return cb(null, info);
            }

        });
    }
}
