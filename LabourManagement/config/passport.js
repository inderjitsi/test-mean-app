
module.exports = function(app, express, passport) {
    var userModel = require('./../models/userModel.js');
    var LocalStrategy = require('passport-local').Strategy;
    var BearerStrategy = require('passport-http-bearer').Strategy;
    var FacebookStrategy = require('passport-facebook').Strategy;
    var GoogleStrategy   = require('passport-google-oauth').OAuth2Strategy;
    var configAuth = require('./social-authentication'); 
    var config = require('../config.json');
    var jwt = require('jsonwebtoken');
    var userService = require('../services/user.service');

    passport.authenticate('LocalStrategy', {
        session: true
    })
    passport.use('local', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        function(req, email, password, authCheckDone) {
            userModel.findOne({
                "email": email,
                "isDeleted": false,
                "isActive": true
            }, function(err, userData) {
                if (err) {
                    return authCheckDone(err);
                } else {
                    if (userData == null) {
                        return authCheckDone(null, false);
                    } else {

                        return authCheckDone(null, {
                            user: userData
                        })
                    }
                }
            })
    }));


  
    passport.use('bearer', new BearerStrategy(function(token, done) {
        process.nextTick(function() {
            userModel.findOne({
                "token": token
            }, function(err, data) {
                if (err) {
                    return done(err);
                } else {
                    if (data == null) {                        
                        return done(null, false);
                    } else {
                        return done(null, {
                            user: data
                        })
                    }
                }
            })
        })
    }));    


    // =========================================================================
    // FACEBOOK ================================================================
    // =========================================================================
    var fbStrategy = configAuth.facebookAuth;
    fbStrategy.passReqToCallback = true;  // allows us to pass in the req from our route (lets us check if a user is logged in or not)
    passport.use(new FacebookStrategy(fbStrategy,
    function(req, token, refreshToken, profile, done) {
        // asynchronous
        process.nextTick(function() {

            saveUser(req, token, refreshToken, profile, done);
        });

    }));

    // =========================================================================
    // GOOGLE ==================================================================
    // =========================================================================
   
   passport.use(new GoogleStrategy({
        clientID        : configAuth.googleAuth.clientID,
        clientSecret    : configAuth.googleAuth.clientSecret,
        callbackURL     : configAuth.googleAuth.callbackURL,
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

    },
    function(req, token, refreshToken, profile, done) {
        // asynchronous
        process.nextTick(function() {
            saveUser(req, token, refreshToken, profile, done);
          
        });

    }));


   function saveUser(req, token, refreshToken, profile, done){
            userService.saveUserForPassport(profile).then(function(user){
                return done(null, user);
            }).catch(function(error){
                return done(err);  
            })
           
   }




    passport.serializeUser(function(adminLoginObj, done) {
        done(null, adminLoginObj);
    });

    passport.deserializeUser(function(adminLoginObj, done) {
        done(null, adminLoginObj);
    });

}




