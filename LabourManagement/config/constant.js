var messages = {
    "FIRST_NAME":"FirstName is required.",
    "LAST_NAME":"LastName is required.",
    "EMAIL":"Email is required.",
    "EMAIL_EXIST":"Email already exists.",
    "USERNAME_EXIST":"Username already exists",
    "INVALIDATE_DETAILS":"Username or Password is incorrect.",
    "ERROR":"Error.",
    "SUCCESS":"Success.",
    "SAVE_DATA":"Data successfully saved.",
    "SAVE_ERROR":"Problem saving data.",
    "UPDATE_DATA":"Data updated successfully.",
    "UPDATE_ERROR":"Error updating data.",
    "STATUS_DATA":"Status changed successfully.",
    "STATUS_ERROR":"Problem changing status.",
    "LIST_DATA":"Data listed successfully.",
    "LIST_ERROR":"Error retrieving data.",
    "QUOTE_SENT": "Quote has successfully been sent to vendor(s).",
    "CORRECT_PASSWORD":"Password is correct",
    "INCORRECT_PASSWORD":"Incorrect Password",
    "PLAN_EXISTS":"Plan name already exists.",

    quote: {
        "PRICE_QUOTE_SUBMITTED":"Price quote has successfully been submitted"
    },
    vendor:{
        "GET_VENDOR_DATA":"Requested Vendor data loaded successfully."
    },
    locations:{
        "SUCCESS":"Success",
        "ERROR":"Error"
    },
    url:{
        "imageUrl":"http://52.34.207.5:4128",
        "quoteUrl":"http://52.34.207.5:4130/#/quote/",
        "userUrl":"http://52.34.207.5:4130/#/user/",
        "logoUrl":"http://52.34.207.5:4128/images/logo.png",
    },
    taskGroupType:{
        "tag":1,
        "template":2
    },
    taskStatus:{
        "completed":2,
        "upcomming":1
    },
    recurType:{
        "Repeat Every Day":1,
        "Repeat Every Week":2,
        "Repeat Every Month":3,
        "Repeat Every Year":4,
        "Never Repeat":5
    },
    reverseRecurType:{
        1:"Repeat Every Day",
        2:"Repeat Every Week",
        3:"Repeat Every Month",
        4:"Repeat Every Year",
        5:"Never Repeat"
    },
    taskNotification:{
        taskAssign:'taskAssign',
        taskReminder:'taskReminder'
    },
    folders:{
        task_docs:'public/task_docs'
    }
}

module.exports = messages;