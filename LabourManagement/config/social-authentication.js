// expose our config directly to our application using module.exports
var request  = require('request');

module.exports = {
    'facebookAuth' : {
        'clientID'        : '1608565842564324', // your App ID
        'clientSecret'    : '5f560b0f2cc5e7971eb5678f70be35c2', // your App Secret
        'callbackURL'     : 'http://localhost:4128/users/auth/facebook/callback',
        'profileURL': 		'https://graph.facebook.com/v2.5/me?fields=first_name,last_name,email',
        'profileFields'   : ['id', 'email', 'name','photos'] // For requesting permissions from Facebook API

		},
		
		'googleAuth' : {
        'clientID'        : '1033846079272-0vhj9v9usobkt728khc032kchv94rqea.apps.googleusercontent.com', // your App ID
        'clientSecret'    : '-elOqy8EDDJrp3g3BqVkaFjq', // your App Secret
        'callbackURL'     : 'http://localhost:4128/users/auth/google/callback'

		},
        'frontEndUrl' : 'http://localhost:4130/#/login'

};
