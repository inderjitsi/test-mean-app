module.exports = function(config) {
    var mongoose = require('mongoose');
    mongoose.connect(config.database, { useMongoClient: true, reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 1000 });
    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
}