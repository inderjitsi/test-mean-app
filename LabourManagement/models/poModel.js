var mongoose = require('mongoose');

require('mongoose-double')(mongoose);
var SchemaTypes = mongoose.Schema.Types;

var Schema = mongoose.Schema;

var poSchema = new Schema({    
	user_id:{type:Schema.Types.ObjectId,ref:'User',required:true},
    company_id:{type:Schema.Types.ObjectId,ref:'Company',required:true},
    po_number: {type:Number},
	po_date: {type:Date},
	delivery_date: Date,
	location: { type:Object },
	locationObject: {type: Object},
	vendor: { type:Object },
	vendorObject: {type: Object},
	address: {type:String},
	items: {type: Object},
	sub_total: { type: String },	
	tax: { type: String },
	gtotal: { type: String },
	notes: {type:String},
	attachment: {type:String},
	isDeleted: { type: Boolean, default: false },
	poStatus: {type: String, default: "New"},
	sent_to_vendor: { type: Boolean, default: true },
	confirmed_by_vendor: { type: Boolean, default: false },
	confirmed_by_vendor_date: { type: Date },
	approved_by_vendor: { type: Boolean, default: false },
	approved_by_vendor_date: {type:Date},
	modified_on: { type: Date, default: Date.now },
	created_date: { type: Date, default: Date.now }
});

module.exports = mongoose.model('PurchaseOrder', poSchema);