var poModel = require('models/poModel.js');
var ObjectId = require('mongoose').Types.ObjectId; 
var poQuoteService = require('services/poQuoteService');
var emails = require('config/emails/emailTemplate.js');
var poController = require('controllers/purchaseOrderController');
var custom = require('custom');

/*________________________________________________________________________________*
@Date:  06 February, 2018*
@Method : create*
Created By: smartData Enterprises Ltd*
Modified On: -*
@Purpose: Function to create new purchase order
Parameters : payload
.__________________________________________________________________________________*/

exports.create = payload => {	

	return getNextPONumber(payload.company_id)
		.then((po_number)=>{

			payload.po_number = po_number;

			payload.poStatus = payload.sent_to_vendor?'Needs Confirmation':'New';

			return new Promise((resolve, reject) => {

				poModel.create( payload, function(err, data) {
					if(err){
						reject(err);
					}else{
						var res_data = { _id: data._id };

						var poQuotePayload = {};
		    			poQuotePayload.company_id = payload.company_id;
		    			poQuotePayload.purchase_order_id = data._id;
		    			poQuotePayload.vendorObject = payload.vendorObject;
		    			poQuotePayload.location = payload.location[0].display;
		    			poQuotePayload.delivery_date = payload.delivery_date;
		    			poQuotePayload.po_number = payload.po_number;

		    			sendPurchaseOrderPdfEmail( poQuotePayload );

						if( payload.sent_to_vendor ){

			    			poQuoteService.create( poQuotePayload )
			    				.then(function(data){ 					
									resolve(res_data);
			    				})
			    				.catch(function(error){
									reject( {error:true, message: "Purchase order was created but not sent to vendor for verification"} );		
			    				})
						}else{
							resolve( res_data );
						}
					}		
				});
			});
		});		
}
//END

/*
* this function sends purchase order as pdf to vendor when new purchase order is created..
*/
const sendPurchaseOrderPdfEmail = payload => {

	let data =	{  
					data: { 	
							subject: payload.location + ' Purchase Order # '+payload.po_number+' – Delivery ' + custom.formatDate( payload.delivery_date ),
     						email: payload.vendorObject[0].email,
     						body: 'Please process the purchase order attached in pdf format' 
     					},     					
  					query: { 
  							_id: payload.purchase_order_id 
  						} 
  				};

	poController.sendEmail(data)
		.then( data => {
			// worked...
		});

}


const getNextPONumber = company_id => {

	return new Promise((resolve)=>{

		poModel.findOne({company_id : new ObjectId(company_id)}, { po_number: 1, _id: 0 }).sort({_id:-1}).limit(1).exec(function(error, result){
			if(error){				
			}else{
				if ( typeof(result) !== "undefined" && result !== null ) {
					resolve(result.po_number+1);
				}else{				
					resolve(1);
				}
			}		
		});
	})	
}


exports.update = (payload) => {

	return new Promise(
		(resolve, reject) => {
			poModel.update( payload.query, 
				{$set: payload.data }, 
				function(err, data) {
				if(err){					
					reject(err);
				}else{					
					resolve(data);
				}
			});
		}
	);
}

exports.email = (payload) => {
	return new Promise(
		(resolve, reject) => {				
	        emails("sendPOEmail", payload, function(err, success) {
	            if (err) {
	                reject(err);
	            } else {
	                resolve(success);
	            }
	        });
		}
	);
}

/*________________________________________________________________________________*
@Date:  10 February 2018*
@Method : getAll*
Created By: smartData Enterprises Ltd*
Modified On: -*
@Purpose: 
@params : payload
.__________________________________________________________________________________*/
exports.getAll = (payload) => {
    
	var sort = payload.sort;

	var count = parseInt(payload.count);

	var query = payload.query;
	query.company_id = payload.company_id;
	
	return new Promise( (resolve, reject) => {

		poModel.find(query).sort(sort).and(payload.filter).skip(payload.skipNo).limit(count).exec(function(err, data) {
		
			if (data) {	
				poModel.find({company_id : new ObjectId(payload.company_id)}).and(payload.filter).count().exec(function(error, totalRecords) {
	
					if (error) {
						reject(error);
					}else{	
						var objResponse = {res: data, totalRecords: totalRecords };	
						resolve(objResponse);
					}
				});
			} else {				
				reject(err);
			}
		})
	});	
}

exports.get = (payload) => {
	return new Promise( (resolve, reject) => {
		poModel.findOne( payload, 
			function(err, data) {
			if(err){
				reject(err);
			}else{
				resolve(data);
			}
		});
	});
}
//END