var config = {
  'staging' : {
      'database': '******',
      "apiUrl": "******",
      "serverUrl":"****"
  },
  'development' : {
      'database': '*******',
      "apiUrl": "******",
      "serverUrl":"******"
  }
}
exports.get = function (env){
  return config[env];
}

exports.rootPath = () => {
  return __dirname;
}
