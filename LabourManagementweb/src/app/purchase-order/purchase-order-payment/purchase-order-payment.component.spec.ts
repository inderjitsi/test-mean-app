import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseOrderPaymentComponent } from './purchase-order-payment.component';

describe('PurchaseOrderPaymentComponent', () => {
  let component: PurchaseOrderPaymentComponent;
  let fixture: ComponentFixture<PurchaseOrderPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseOrderPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseOrderPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
