import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { DatepickerOptions } from 'ng2-datepicker';
import * as frLocale from 'date-fns/locale/fr';
import {PurchaseOrderService} from '../purchase-order.service';
import { ActivatedRoute } from '@angular/router';
import {appConfig} from '../../app.config';
import { Custom } from '../../classes/custom';
import { SweetAlertService } from '../../_services/sweetalert.service';

@Component({
  selector: 'app-purchase-order-payment',
  templateUrl: './purchase-order-payment.component.html',
  styleUrls: ['./purchase-order-payment.component.css']
})
export class PurchaseOrderPaymentComponent implements OnInit {

	@Input() bills;
	@Input() payment: any = {};
	@Input() index;
  @Input() poId;
  file: any = {};
  @Output() onSave = new EventEmitter();
  @Output() delete = new EventEmitter();
  @Output() pdfEvent = new EventEmitter< any >();
  @Output() showMessage = new EventEmitter <any>();
  @Output() emailEvent = new EventEmitter <any>();

  public custom_class = new Custom();

  public billsCopy: any = [];

  apiUrl = appConfig.apiUrl;

	 options: DatepickerOptions = {
    minYear: 1970,
    maxYear: 2018,
    displayFormat: 'MMM D[,] YYYY',
    barTitleFormat: 'MMMM YYYY',
    firstCalendarDay: 0, // 0 - Sunday, 1 - Monday
    locale: frLocale,
    minDate: new Date(Date.now()), // Minimal selectable date
    maxDate: new Date(Date.now())  // Maximal selectable date
  };

 	status: [{value: 'pending', display: 'pending'}, {value: 'paid', display: 'paid'}]

  constructor(
    private purchaseOrderService: PurchaseOrderService,
    private alertService: SweetAlertService,
    private route: ActivatedRoute ) {
  }

  ngOnInit() {

    this.payment.status = (this.payment.paid) ? this.payment.paid : false;

    let arr = this.createPaymentInputs();

    if (arr.length > 1){

      let found = arr.findIndex( element => {
              return (element.id !== 'all')
            });
      console.log( "found", found );

      if( !found ){
        arr.unshift({name: 'All', 'id': 'all', 'display': 'All', value: 'all'});
      }    
    }

    this.billsCopy = arr;    

    console.log( "this.billsCopy", this.billsCopy  );    

  	this.setDefault();
  }

  setDefault(){
  	if (!this.payment){
  		this.payment = {};
  	}
  	this.payment.payment_date = this.payment.payment_date ? this.payment.payment_date : new Date();
  	this.payment.total = this.payment.total ? this.payment.total : 0;

  	 this.bills.map((data) => {
        data.display = data.bill_number + '';
        data.value = data._id;
      })
    this.filterBills();
  }

  /*
  * create Receipt Tag Inputs
  */
  createPaymentInputs(){

    return this.bills.filter(function(bill){

      if ('_id' in this ){

        const k = this.bills.find((data) => {
          return data._id === bill._id;
        });

        if ( typeof k === 'undefined' &&  bill.paid == false ){
          return true;
        }else if (typeof k !== 'undefined'){
          return true;
        }

      }else{
        return bill.paid == false;
      }

    }, this.payment );
  }

  fileChange(event){
    this.file = event.target.files && event.target.files ? event.target.files[0] : null;
  }

  savePayment(){
     const formData = new FormData();
        if (this.file){
            formData.append('files', this.file);
        }

        this.payment.po_id = this.poId;
        this.payment.paid = this.payment.status;

        formData.append('payload', JSON.stringify(this.payment));

        this.purchaseOrderService.savePoPayment(formData).subscribe(

          (data) => {
            // show message
            const msgReturn = {'message': 'Payment has been created successfully!', 'type': 'success' };
            this.showMessage.emit(msgReturn);

            this.onSave.emit({data: data, type: 'payment'});
            this.filterBills();
            this.cancel();
          },
          (error) => {
            const msgReturn = {'message': 'Something went wrong, please try later!', 'type': 'error' };
            this.showMessage.emit(msgReturn);
          });
  }

  /*
  * Edit payment
  */
  editPayment(){
    const formData = new FormData();

    if (this.file){
        formData.append('files', this.file);
    }

    this.payment.po_id = this.poId;
    this.payment.paid = this.payment.status;

    formData.append('payload', JSON.stringify(this.payment));
    this.purchaseOrderService.updatePoPayment(formData).subscribe(
    (data) => {
      this.onSave.emit({data: data, type: 'payment'});
      const msgReturn = {'message': 'Payment has been updated successfully!', 'type': 'success' };
      this.showMessage.emit(msgReturn);
    },
    (error) => {
      const msgReturn = {'message': 'Something went wrong, please try later!', 'type': 'error' };
      this.showMessage.emit(msgReturn);
    })
  }

  cancel(){
    this.delete.emit({index: this.index, type: 'payment'});
  }

  filterBills(){
    this.bills = this.bills.filter((data) => {
      return data.paid == false;
    })
  }
  
  onAdding(data: any){

    if(data.id == 'all'){

      this.payment.bills = [];

      const items = this.billsCopy.filter( data => {
         return data.value != 'all';
      });
      this.payment.bills = items;
    }

    this.payment.total = 0;
    this.calculateTotal();
  }

  onRemoving (data: any) {
  }

  calculateTotal(){
     if (this.payment.bills && this.payment.bills){
      this.payment.bills.map((data) => {
        this.payment.total = this.payment.total  + data.total;
      })
     }
  }

  onPdf(){
    const endpoint = '/pdf/payment';
    const query = {_id: this.payment._id };
    const obj = { endpoint: endpoint, query: query };

    this.pdfEvent.emit(obj);
  }

  /*
  *  open email modal in the parent component...
  */
  onEmail(){

    const obj = {
      endpoint: '/po-payment/email',
      query: {
        _id: this.payment._id
      },
      subject: ' Payment #' + this.payment.payment_number + ' – Payment Date ' + this.custom_class.formatDate(this.payment.payment_date)
    };

    this.emailEvent.emit( obj );
  }

  /*
  * Delete Receipt
  */
  onDelete() {

    this.alertService.confirm({
      title: 'Are you sure?',
      text: 'This action can\'t be undone',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      // Yes, delete it
      if (result.value) {
        this.deletePayment();        
      }
      // No, keep it
      if (result.dismiss === "cancel") {        
      }
    });
  }

  /*
  * delete payment
  */
  deletePayment() {
    
    let query = {
      _id: this.payment._id
    };

    this.purchaseOrderService.deletePayment(query).subscribe(
      data => {
        const msgReturn = {'message': 'Payment has been deleted successfully!', 'type': 'success', 'deleted':'payment' };
        this.showMessage.emit(msgReturn);
      },
      error => {        
        const msgReturn = {'message': 'Something went wrong, please try later!', 'type': 'error' };
        this.showMessage.emit(msgReturn);
      }
    );
  }


}
