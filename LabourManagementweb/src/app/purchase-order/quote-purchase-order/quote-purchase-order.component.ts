import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PO, Address, POItem } from '../../_models/purchase-order';
import { PoQuoteService } from '../../_services/poquote.service';
import { PurchaseOrderService } from '../purchase-order.service';
import { SweetAlertService } from '../../_services/sweetalert.service';
import { ToastsManager, Toast } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-quote-purchase-order',
  templateUrl: './quote-purchase-order.component.html',
  styleUrls: ['./quote-purchase-order.component.css']
})
export class QuotePurchaseOrderComponent implements OnInit {

	public urlKey: string;
	//public purchaseOrder: PO = new PO();
	public purchaseOrder: any = {};
	// creae new po form
	poForm: FormGroup;

	public isLoading = false;

	public taxPercentage = 0;

	constructor(
		private fb: FormBuilder,
		private route: ActivatedRoute,
		public toastr: ToastsManager,
		public vcr: ViewContainerRef,
		private router: Router,
		private alertService: SweetAlertService,
		private poQuoteService: PoQuoteService,
		private poService: PurchaseOrderService
		) {
		this.toastr.setRootViewContainerRef(vcr);
    this._createPOForm();
	}

	ngOnInit() {
		this._qetQuoteUrl();
	}

	private _delay = (ms: number) => {
	    return new Promise ( (resolve) => setTimeout(resolve, ms) );
	}

	private _qetQuoteUrl(){

		this.urlKey = this.route.snapshot.params['url'];

		if (typeof this.urlKey !== 'undefined'){

			this._getQuoteByUrl( { url: this.urlKey} );
		}
	}

	/*
	* get the quote data based on the requested url..
	*/
	private _getQuoteByUrl( apiData: any){

		this.poQuoteService.findOne( apiData ).subscribe(

        data => {
  	      this.purchaseOrder = data;
  	      //this._createPOForm();
          this._setFormValues(data);
          this._setItems(this.purchaseOrder.purchase_order_id.items);
          // set validations for items array
          this._setItemsValidations();
        },
        error => {
        	this._delay(100).then(() => {
  		    this.alertService.error({ title: 'Oops!',
            	text: 'This url has been expired' });
  		      this.router.navigate(['/404']);
  	      });
        }
    );
	}

	/*
  * create poForm
  */
  private _createPOForm(){
    this.poForm = this.fb.group({
      items: this.fb.array([]),
      sub_total: '',
      tax: '',
      gtotal: ''
    });
  }

  /*
  * set form values
  */
  private _setFormValues(data: any){
    this.poForm.patchValue({
      sub_total: data.purchase_order_id.sub_total,
      tax: data.purchase_order_id.tax,
      gtotal: data.purchase_order_id.gtotal
    });
  }

  get items(): FormArray {
    return this.poForm.get('items') as FormArray;
  };

  /*
  * set validations for items array
  */
  private _setItemsValidations(){

    const control = <FormArray>this.poForm.get('items');

    const sub_total = 0;

    for (const i in control.value ){
      control.controls[i].get('price').setValidators(Validators.required);
    }
  }

  private _setItems(items: any){
  	const fGroup = items.map( data => this.fb.group(data) );
  	const fArray = this.fb.array(fGroup);
  	this.poForm.setControl('items', fArray );
  }

  /*
  * set po totals
  */
  calLineTotal( index: number ){

    const control = <FormArray>this.poForm.get('items');
    const obj = control.value[index];

    const quantity = parseInt(obj.quantity);
    const price = parseFloat(obj.price);
    const total = quantity * price;

    control.controls[index].get('total').setValue(total.toFixed(2));

    // calculate sub total of all line items...
    this._calSubTotal();

    // recalculate tax on the sub total;
    this.setTax();

    // calculate grand total..
    this._calGrandTotal();

  }

  /*
  * calculate sub total on line total change...
  */
  private _calSubTotal(){

    const control = <FormArray>this.poForm.get('items');

    let sub_total = 0;

    for (const i in control.value ){

      const obj = control.value[i];

      sub_total += parseFloat( obj.total );
    }

    this.poForm.patchValue({
      sub_total: sub_total.toFixed(2)
    });

  }

  setTax = () => {

    const tax_percentage = this.purchaseOrder.purchase_order_id.tax_percentage;

    const sub_total = parseFloat(this.poForm.get('sub_total').value);

    const tax = sub_total * (parseInt(tax_percentage) / 100);

    this.poForm.patchValue({
      tax: tax.toFixed(2)
    });

    // recalculate grand total as tax has been changed..
    this._calGrandTotal();
  }

  /*
  * calculate sub total on line total change...
  */
  private _calGrandTotal(){

    const sub_total = parseFloat(this.poForm.get('sub_total').value);

    const tax = parseFloat(this.poForm.get('tax').value);

    const gtotal = sub_total + tax;

    this.poForm.patchValue({
      gtotal: gtotal.toFixed(2)
    });
  }

  onSubmit(){

  	const obj2 = {approved_by_vendor: true, approved_by_vendor_date: new Date() };

  	const payload = Object.assign(this.poForm.value, obj2);

  	const data = {
  		query: { _id: this.purchaseOrder.purchase_order_id._id},
  		data: payload
  	};

    const message = 'The Pricing changes have been submitted. Please wait to process the order.\n\nThis page has been expired';

  	this._updatePurchaseOrder(data, message);
  }

  /*
  * confirm will set the key "confirmed" true
  */
  public confirm = () => {

	  const data = {
      query: { _id: this.purchaseOrder.purchase_order_id._id },
			data: {
        poStatus: 'Processing',
				confirmed_by_vendor: true,
				confirmed_by_vendor_date: new Date()
			}
		};

    const message = 'Please Continue Processing the Order. \n\nThis page has been expired';

		this._updatePurchaseOrder(data, message);
  }

  private _updatePurchaseOrder(data: any, message: string){
  	this.isLoading = true;
  	this.poService.update( data ).subscribe(
  		(data) => {
  			this.isLoading = false;
  			this._deleteQuote(message);
  		},
  		(error) => {
  			this.isLoading = false;
  			this.toastr.error('Something went wrong, please try later', 'Oops!');
  		}
  	);
  }

  private _deleteQuote = (message: string) => {

  	const obj = {_id: this.purchaseOrder._id };

  	this.poQuoteService.delete(obj).subscribe(
  		(data) => {
  			this._delay(3000).then(() => {
  				this.alertService.success({ title: 'Success!',
  					text: message });
  				this.router.navigate(['/404']);
  			});
  		},
  		(error) => {
  			console.error('error delete', error );
  		}
  	);
  }

}
