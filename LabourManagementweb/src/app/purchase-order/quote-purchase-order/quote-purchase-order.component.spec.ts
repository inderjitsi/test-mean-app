import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuotePurchaseOrderComponent } from './quote-purchase-order.component';

describe('QuotePurchaseOrderComponent', () => {
  let component: QuotePurchaseOrderComponent;
  let fixture: ComponentFixture<QuotePurchaseOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuotePurchaseOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuotePurchaseOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
