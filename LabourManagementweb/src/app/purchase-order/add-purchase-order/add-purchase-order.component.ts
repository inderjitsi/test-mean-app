import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { DatepickerOptions } from 'ng2-datepicker';
import * as frLocale from 'date-fns/locale/fr';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastsManager, Toast } from 'ng2-toastr/ng2-toastr';
import { TooltipModule } from "ngx-tooltip";

import { PO, Address, POItem } from '../../_models/purchase-order';
import { PurchaseOrderService } from '../purchase-order.service';
import { VendorService } from '../../_services/vendor.service';
import { LocationService } from '../../_services/location.service';
import { ItemService } from '../../_services/item.service';
import { CrudService } from '../../_services/crud.service';
import { Custom } from '../../classes/custom';

@Component({
  selector: 'app-add-purchase-order',
  templateUrl: './add-purchase-order.component.html',
  styleUrls: ['./add-purchase-order.component.css']
})
export class AddPurchaseOrderComponent{
  public address: Object;
  data: any = {};
  locationArray: any;
  vendorArray: any;
  public itemsArray = [];
  selectedLocation: string;
  location: any;
  po = new PO();
  fileList: any;
  fileName: String;
  public locationTags: any = [];
  public vendorTags: any = [];
  public isLoading = false;
  public unitsArray = [];
  info: any = [];

  // edit
  public po_id = '';
  public po_modal: any = {};
  public templates: any = [];
  public objCustom = new Custom();

  /// 
  total_tax_amount: any = '';
  total_price: any = '';
  total_quantity: any = '';

  // create new po form
  poForm: FormGroup;

  options: DatepickerOptions = {
    minYear: 1970,
    maxYear: new Date(Date.now()).getFullYear(),
    displayFormat: 'MMM D[,] YYYY',
    barTitleFormat: 'MMMM YYYY',
    firstCalendarDay: 0, // 0 - Sunday, 1 - Monday
    locale: frLocale,
    minDate: new Date(Date.now()), // Minimal selectable date
    maxDate: new Date(Date.now())  // Maximal selectable date
  };

  constructor(
    private poService: PurchaseOrderService,
    private vendorService: VendorService,
    private locationService: LocationService,
    private itemService: ItemService,
    private fb: FormBuilder,
    public toastr: ToastsManager,
    public vcr: ViewContainerRef,
    private route: ActivatedRoute,
    private crudService: CrudService,
    private router: Router    
    ) {
    this.toastr.setRootViewContainerRef(vcr);
    this._createPOForm();
  }

  ngOnInit() {

    this.getAllLocations();
    this.getAllVendors();

    this.po_id = (this.route.snapshot.params['id']) ? this.route.snapshot.params['id'] : '';

    if (this.po_id != ''){
      this._get()
        .then( data => {
          this._setFormControlValues();
        });
    }else{
      this._getTemplates();
    }
  }

  private _getTemplates(){

    const queryObject = { query: { isDeleted: false }, columns: {title: 1 } };

    this.crudService.findMany( '/po-templates/findMany',  queryObject ).subscribe(
      (data) => {
        this.templates = data;
      }
    );
  }

  /*
  * get purchase order template
  */
  private _get(){

    return new Promise( (resolve) => {

      this.isLoading = true;

      this.poService.get('/purchase-orders/get', {_id: this.po_id}).subscribe(
        data => {
          this.po_modal = data;
          this.isLoading = false;
          resolve(data);
        }
      );
    });
  }


  /*
  * set form control values in edit view...
  */
  private _setFormControlValues(){

    const form_controls = [ 'po_number',
                          'po_date',
                          'delivery_date',
                          'location',
                          'vendor',
                          'address',
                          'sub_total',
                          'tax_percentage',
                          'tax',
                          'gtotal',
                          'notes',
                          'sent_to_vendor'
                        ];

    for (const control of form_controls ){
      if ( this.po_modal[control] ){
        this.poForm.controls[control].setValue( this.po_modal[control] );
      }
    }

    this._setFGArray(this.po_modal.items , 'items');
    this._setFGArray(this.po_modal.locationObject , 'locationObject');
    this._setFGArray(this.po_modal.vendorObject , 'vendorObject');

    this.getAllItems()
      .then( data => {
        // set items' unit
        for (const i in this.po_modal.items ){
          this.setItem(i, this.po_modal.items[i].id );
        }

        this.calculateTotals();

      });
  }


  fileChange(event) {
    this.fileList = event.target.files;
    this.fileName = event.target.files[0].name;
  }


  /*
  * create new purchase order form
  */
  private _createPOForm() {

    this.poForm = this.fb.group({
      po_number:  '',
      po_date: [new Date(), Validators.required ],
      delivery_date: [this._addDays(new Date(), 5), Validators.required ],
      location: ['', Validators.required ],
      locationObject: this.fb.array([]),
      vendor: ['', Validators.required ],
      vendorObject: this.fb.array([]),
      address: ['', Validators.required ],
      items: this.fb.array([]),
      sub_total: ['', Validators.required ],
      tax_percentage: 0,
      tax: ['', Validators.required ],
      gtotal: ['', Validators.required ],
      notes: [''],
      sent_to_vendor: false
    });

    //this.items.push(this.fb.group(new POItem()));

    // set validations for items array
    //this._setItemsValidations();

  }


  /**
   * @param: theDate, days
   */
  private _addDays = (theDate, days) => {
      return new Date(theDate.getTime() + days * 24 * 60 * 60 * 1000);
  }

  get items(): FormArray {
    return this.poForm.get('items') as FormArray;
  };

  /*
  * add new item row
  */
  addItem() {

    //this.poForm.get('name').setValidators(Validators.required);

    this.items.push( this.fb.group( new POItem() ) );

    this._setItemsValidations();
  }
  
  /*
  * set validations for items array
  */
  private _setItemsValidations() {

    const control = <FormArray>this.poForm.get('items');

    const sub_total = 0;

    for (const i in control.value ){
      control.controls[i].get('price').setValidators(Validators.required);
      control.controls[i].get('unit').setValidators(Validators.required);
      control.controls[i].get('quantity').setValidators(Validators.required);
      control.controls[i].get('name').setValidators(Validators.required);
      control.controls[i].get('total').setValidators(Validators.required);
    }
  }

  /*
  * remove item row
  */
  removeItem( index: number ){
    const control = <FormArray>this.poForm.controls['items'];
    control.removeAt(index);
    this.calculateTotals();
  }

  /*
  * calculate tax when item is removed...
  */
  calculateTotals() {
    // calculate sub total of all line items...
    this._calSubTotal();

    // recalculate tax on the sub total;
    //this.setTax();

    // calculate grand total..
    this._calGrandTotal();
  }


  getAllLocations(){

    this.locationService.getAll( { isDeleted: false } ).subscribe(

      data => {
        this.locationArray = data.res;
        data.res.map((data) => {
          this.locationTags.push({
            value : data._id,
            display: data.title
          })
        })
      },
      error => {
         console.error('error', error);
      }
    );
  }

  getAllVendors(){
    this.vendorService.getAll( { isDeleted: false } ).subscribe(
      data => {
        this.vendorArray = data.res;
        data.res.map(
          (data) => {
            this.vendorTags.push({
              value: data._id,
              display: data.name
            })
          }
        );
      },
      error => {
        console.error('error', error);
      }
    );
  }

  getAllItems() {

    return new Promise((resolve, reject) => {
      const location = this.poForm.get('location').value;

      const queryObject = { query:
        {
          isDeleted: false,
          'location': {
            $elemMatch: {'value': location[0].value}
          }
        },
        columns: {}
      };

      this.crudService.findMany( '/items/findMany',  queryObject ).subscribe(
        (data) => {
          this.itemsArray = data.map(item => {
            return Object.assign({ id: item._id }, item);
          });
          resolve(data);
        }
      );
    });
  }


  /*
  * set address when location is selected
  */
  setAddress(){

    const locationControl = this.poForm.controls['location'].value;

    const location_id = ( locationControl[0] ) ? locationControl[0].value : '';

    const resultObject = this._searchObjectInArray(location_id, this.locationArray );

    const arr = [];

    let address = '';

    if ( typeof resultObject !== 'undefined' ){
      address = this._formatShippingAddress(resultObject);

      arr.push(resultObject);
    }

    this._setFGArray(arr, 'locationObject');

    this.poForm.patchValue({
      address: address
    });
  }

  //
  private _formatShippingAddress = (resultObject: any ) => {
    return resultObject.shipping.address + ', ' + resultObject.shipping.city + ', ' + resultObject.shipping.state + ', ' + resultObject.shipping.country + ', ' + resultObject.shipping.postal_code;
  }

  /*
  * called when vendor is selected/changed...
  */
  setVendor(){

    const vendorControl = this.poForm.controls['vendor'].value;

    const vendor_id = ( vendorControl[0] ) ? vendorControl[0].value : '';

    const arr = [];

    if ( vendor_id ){
      const obj = this._searchObjectInArray(vendor_id, this.vendorArray );
      arr.push(obj);
    }

    this._setFGArray(arr, 'vendorObject');

    this.setPricesForPOItems();
  }

  // not being used
  setPreferredInfoForPOItems() {

    const control = <FormArray>this.poForm.get('items');

    for (const i in control.value ){

      const obj = control.value[i];

      if( obj.id != '' ) {    
        this.getPreferredPrice(i, obj.id );
      }      
    }
  }

  setPricesForPOItems() {

    const control = <FormArray>this.poForm.get('items');

    for (const i in control.value ){

      const obj = control.value[i];

      if( obj.id != ''  ) {
        this.getBestPrice(obj.unit, obj.id, i);        
      }      
    }
  }



  /*
  *
  */
  private _setFGArray(arrayControl: any, arrayControlName: any){
    const FGs = arrayControl.map(data => this.fb.group(data));
    const formArray = this.fb.array(FGs);
    this.poForm.setControl(arrayControlName, formArray);
  }


  /*
  * get the locaiton object by its id
  */
  private _searchObjectInArray(nameKey, myArray){

    for (let i = 0; i < myArray.length; i++) {

        if (myArray[i]._id === nameKey) {
            return myArray[i];
        }
    }
  }

  /*
  * this function is called when item is selected from the dropdown
  */
  setItem(index: any, item_id: string) {

    const resultObject = this._searchObjectInArray( item_id, this.itemsArray );
      
    this.getItemTax( resultObject )
      .then( tax_percentage => {
          const control = <FormArray>this.poForm.get('items');
          control.controls[index].get('tax').setValue( tax_percentage );
        });

    const unitsArray = this.objCustom.getItemUnits(resultObject, this.unitsArray );

    this.unitsArray = [];

    this.unitsArray = unitsArray;

    const control = <FormArray>this.poForm.get('items');

    control.controls[index].get('description').setValue( (resultObject.description)?resultObject.description:'' );
    control.controls[index].get('name').setValue( resultObject.name );

    this._setItemsValidations();

    let unit = control.controls[index].value['unit'];

    if( unit != "" ){

      console.log("resultObject.id", resultObject.id );

      this.getBestPrice(unit, resultObject.id, index);
    }   

    this.getPreferredPrice(index, resultObject._id );
    this.calLineTotal( index ); 
  }


  /*
  * get item tax
  */
  getItemTax( item : any ) {

    return new Promise( (resolve) => {
      let classification_ids = [];

      classification_ids.push( item.department[0].value, item.class[0].value, item.category[0].value, item.sub_category[0].value );

      this.getTaxes(classification_ids)
        .then( data => {
          let tax = this.getTax(data);
          if(tax instanceof Object ){
            resolve(tax.amount);
          }else{
            resolve(0);
          }
        });
    });
  }


  getTax(taxes: any ) {

    let sub_category_tax = (taxes.find( tax => {
          return tax.relation[0].tag == 'sub_category';
        }));
    if( typeof sub_category_tax != 'undefined' ){
      return sub_category_tax;
    }

    let category_tax = (taxes.find( tax => {
          return tax.relation[0].tag == 'category';
        }));
    if( typeof category_tax != 'undefined' ){
      return category_tax;
    }

    let class_tax = (taxes.find( tax => {
          return tax.relation[0].tag == 'class';
        }));
    if( typeof class_tax != 'undefined' ){
      return class_tax;
    }    

    let department_tax = (taxes.find( tax => {
          return tax.relation[0].tag == 'department';
        }));
    if( typeof department_tax != 'undefined' ){
      return department_tax;
    }

    return false;
  }

  /*
  * get taxes (department, class, category, subcategory) based on classification ids array
  */
  getTaxes(classificationIdsArray: any ) {

    return new Promise( (resolve) => {

       const queryObject = { query:
        {
          status: true,
          'relation': {
            $elemMatch: {'value': { $in: classificationIdsArray } }
          }
        },
        columns: { amount: 1, relation:1 }
      };

      this.crudService.findMany( '/tax/findMany',  queryObject ).subscribe(
        data => {
          resolve(data);
        }
      );
    });
  }


  /*
  * set po totals
  */
  calLineTotal( index: number ) {

    const control = <FormArray>this.poForm.get('items');
    const obj = control.value[index];
    const tax_percentage = ( parseFloat(obj.tax) )?parseFloat(obj.tax):0;
    const quantity = parseInt(obj.quantity);
    const price = parseFloat(obj.price);
    
    const total = quantity * price ? quantity * price : 0;

    const item_tax_amount = total * (tax_percentage/100);    

    const total_item_amount = total + item_tax_amount;

    control.controls[index].get('tax_amount').setValue(item_tax_amount.toFixed(2));

    control.controls[index].get('total').setValue(total_item_amount.toFixed(2));

    this.calculateTotals();
  }


  /*
  * calculate sub total on line total change...
  */
  private _calSubTotal(){

    const control = <FormArray>this.poForm.get('items');

    let sub_total = 0;
    let total_tax_amount = 0;
    let total_price = 0;
    let total_quantity = 0;

    for (const i in control.value ){

      const obj = control.value[i];

      let sub_total_cal = parseFloat( obj.price ) * parseInt(obj.quantity) ;

      sub_total +=  (sub_total_cal) ? sub_total_cal : 0;

      total_tax_amount += (parseFloat( obj.tax_amount )) ? parseFloat( obj.tax_amount ) : 0;
      total_price += (parseFloat( obj.price )) ? parseFloat( obj.price ) : 0;
      total_quantity += (parseFloat( obj.quantity )) ? parseFloat( obj.quantity ) : 0;
    }

    this.total_tax_amount = total_tax_amount.toFixed(2);
    this.total_price = total_price.toFixed(2);
    this.total_quantity = total_quantity;

    this.poForm.patchValue({
      sub_total: sub_total.toFixed(2),
      tax: this.total_tax_amount
    });
  }

  
  private _calGrandTotal(){

    const sub_total = parseFloat(this.poForm.get('sub_total').value);

    const tax = parseFloat(this.poForm.get('tax').value);

    const gtotal = (sub_total + tax) ? sub_total + tax : 0;

    this.poForm.patchValue({
      gtotal: gtotal.toFixed(2)
    });
  }

  onSubmit(){
    // check whether to update or add new one....
    if ( this.po_modal._id ){
      this._update();
    }else{
      this._create();
    }
  }


  private _setItemIds(){
    const items = <FormArray>this.poForm.get('items');

    for (const i in items.value ){
      items.controls[i].get('item_id').setValue( parseInt(i) + 1 );
    }
  }

  private _create(){
    this.isLoading = true;

    // set item_id for all po items..
    this._setItemIds();

    const formData = new FormData();

    const file = this.fileList && this.fileList.length ? this.fileList[0] : null;

    if (file){
        formData.append('files', file);
    }

    formData.append('payload', JSON.stringify( this.poForm.value ) );

    this.poService.create(formData).subscribe(

      data => {
        this.poForm.reset();
        this.toastr.success('Purchase order has been created successfully!', 'Success!');
        this.isLoading = false;
        this.router.navigate(['/purchase-order/view', data._id]);
      },
      error => {
        this.toastr.error('Something went wrong. Purchase order was not created, please try later', 'Oops!');
        this.isLoading = false;
      }
    );
  }

  private _update(){

    this.isLoading = true;

    // set item_id for all po items..
    this._setItemIds();

    const formData = new FormData();

    const file = this.fileList && this.fileList.length ? this.fileList[0] : null;

    if (file){
      formData.append('files', file);
    }

    const objApiData = {};

    objApiData['query'] = {_id: this.po_modal._id};
    objApiData['data'] = this.poForm.value;

    formData.append('payload', JSON.stringify( objApiData ) );

    this.poService.update( formData ).subscribe(

      data => {
        this.toastr.success('Purchase order has been updated successfully!', 'Success!');
        this.isLoading = false;
         this.router.navigate(['/purchase-order/list']);
      },
      error => {
        this.toastr.error('Something went wrong. Purchase order was not created, please try later', 'Oops!');
        this.isLoading = false;
      }
    );
  }


  setPOFromTemplate(template_id: string){
    if (template_id != ''){
      this._getTemplate(template_id)
      .then((data) => {
        this._setFormControlValues();
      });
    }
  }

  /*
  * get purchase order template
  */
  private _getTemplate(template_id: string){

    return new Promise( (resolve) => {

      this.isLoading = true;

      this.crudService.get('/po-templates/get', { _id: template_id }).subscribe(
        data => {
          delete data['_id'];
          this.po_modal = data;
          this.isLoading = false;
          resolve();
        },
        error => {
          this.isLoading = false;
        }
      );
    });
  }

  /*
  * get min price based on item size & location
  */
  getBestPrice(unit, item_id, index) {

    const location = this.poForm.get('location').value;

    const vendor = this.poForm.get('vendor').value;

    if( vendor.length > 0 && location.length > 0 ){

      const obj = {
        query: {
          item_id: item_id,
          unit: unit,
          location: location[0].value,        
          vendor_id: vendor[0].value
        },
        columns: {}
      }; 

      this.crudService.get( '/item-prices/get',  obj ).subscribe(
        response => {

          const control = <FormArray>this.poForm.get('items');

          if (response){
            control.controls[index].get('price').setValue(response.price);
          }else{
            //control.controls[index].get('price').setValue('');
          }
          this.calLineTotal( index );
          this.getPreferredPrice(index, item_id );
        },
        error => {
          const control = <FormArray>this.poForm.get('items');
          //control.controls[index].get('price').setValue('');
          this.getPreferredPrice(index, item_id );
          this.calLineTotal( index );
        }
      );
    }
  }

  /*
  * on adding location, get all associated items to that location and 
  * remove already added items from the purchase order 
  * if not associated with location...
  */
  onAddingLocation() {
    this.setAddress();
    this.getAllItems()
      .then( data => {
        this.removeItems();
        this.calculateTotals();
        this.setPricesForPOItems();
      });
  }

  onRemovingLocation() {
    this.setAddress();    
  }

  /*
  * It removes items from the purchase order which are not associated witht the selected location...
  */
  removeItems() {
    
    const items = <FormArray>this.poForm.get('items');

    const control = <FormArray>this.poForm.controls['items'];

    for (const i in items.value ){

      let found = this.itemsArray.findIndex( elem => {                
          return (typeof items.controls[i] !== 'undefined') && (elem.id == items.controls[i].value['id']);        
      });

      if( found == -1 ){
        control.removeAt(parseInt(i));        
      }      
    }
  }


  clearFormArray = ( formArrayName: string) => {
    const formArray = this.fb.array([]);
    this.poForm.setControl(formArrayName, formArray);
  }

  /*
  * get preferred price for this item, unit, locaiton..
  */
  getPreferredPrice(index: any, item_id: any ) {

    const control = <FormArray>this.poForm.get('items');
    
    let unit = control.controls[index].value['unit'];

    const vendorControl = this.poForm.controls['vendor'].value;

    const vendor_id = ( vendorControl[0] ) ? vendorControl[0].value : '';

    const locationControl = this.poForm.controls['location'].value;

    const location_id = ( locationControl[0] ) ? locationControl[0].value : '';

    if( location_id != '' && vendor_id != '' && unit != '' ){
      let obj = {
        query: {
          item_id: item_id,
          unit: unit,
          location: location_id,
          best_price_flag: true,
          vendor_id: {$ne: vendor_id} 
        },
        columns: {}
      };

      this.callGetPreferredPriceService(obj, index);
    }    
  }

  callGetPreferredPriceService(obj, index) {

     this.crudService.get( '/item-prices/get',  obj ).subscribe(
      response => {
        let info = "There is preferred price $"+ response.price +" from vendor "+response.vendor_id.name;

        if (response){
          this.info[index] = info;
        }else{
          this.info[index] = '';
        }        
      },
      error => {
        this.info[index] = '';        
      }
    );
  }



}
// component class ends here.
