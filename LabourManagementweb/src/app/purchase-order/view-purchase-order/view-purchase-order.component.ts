import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { BsModalComponent } from 'ng2-bs3-modal';
import { ToastsManager, Toast } from 'ng2-toastr/ng2-toastr';
import { PurchaseOrderService } from '../purchase-order.service';
import { ActivatedRoute } from '@angular/router';
import {appConfig} from '../../app.config';
import { CrudService } from '../../_services/crud.service';
import { Custom } from '../../classes/custom';
import { NgForm } from '@angular/forms';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';

@Component({
  selector: 'app-view-purchase-order',
  templateUrl: './view-purchase-order.component.html',
  styleUrls: ['./view-purchase-order.component.css']
})
export class ViewPurchaseOrderComponent implements OnInit {

  purchaseOrderType: any = [
    {name: 'PO Receipts', value: 'receipt', label: 'PO Receipt'}, 
    {name: 'Bills', value: 'bill', label: 'Bill'}, 
    {name: 'Payments', value: 'payment', label: 'Payment'}];

  poObject: any ;
  poTypes: any = {receipt: [], bill: [], payment: []};
  counts: any = {};
  apiUrl = appConfig.apiUrl;
  purchaseOrderId: any;
  showSave: any = {};
  emailModel = {};
  public itemsTrackData: any = {};

  isLoading = false;
  public emailObject: any;
  public custom_class = new Custom();

  //massUpdateModal settings...
  @ViewChild('emailModal')
  emailModal: BsModalComponent;

  constructor(
    private purchaseOrderService: PurchaseOrderService,
    private crudService: CrudService,
    private route: ActivatedRoute,
    public toastr: ToastsManager,
    public vcr: ViewContainerRef ) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.purchaseOrderId = this.route.snapshot.params['id']
    this.getPurchaseOrder(this.purchaseOrderId )
      .then(() => {
        this.poItemsTrack();
        this.getPoReceipt(this.purchaseOrderId );
        this.getBills(this.purchaseOrderId);
        this.getPayments(this.purchaseOrderId);
      });
  }

  getPurchaseOrder(id) {

    return new Promise((resolve) => {

      this.purchaseOrderService.getPurchaseOrderById(id).subscribe(
      (data) => {
        this.poObject = data;
        resolve();
      },
      (error) => {
        console.error('failed to fetch purchase order');
      });

    });
  }


  addNew(type) {
    if (!this.counts[type]){
      this.counts[type] = [];
    }
    if ( this.counts[type].length == 0 ){
      this.counts[type].push({type: type});
    }
  }

  getPoReceipt(poId) {
    return new Promise((resolve) => {
      this.purchaseOrderService.getPoReceipt(poId).subscribe((data) => {
        this.poTypes['receipt'] = data;
        this.onOpenAccordian();
        resolve(data);
      });
    });
  }

  getBills(poId) {
    return new Promise((resolve) => {
      this.purchaseOrderService.getPoBill(poId).subscribe((data) => {
        this.poTypes['bill'] = data;
        this.onOpenAccordian();
        resolve(data);
      });
    });
  }

  getPayments(poId) {
    return new Promise((resolve) => {
      this.purchaseOrderService.getPoPayment(poId).subscribe((data) => {
        this.poTypes['payment'] = data;
        this.onOpenAccordian();
        resolve(data);
      });
    });
  }


  /*
  * calculate items receivings, billings & payings.
  */
  poItemsTrack() {

    const itemsTrackData = {};

    let subTotalReceived = 0;
    let subTotalBilled = 0;
    let subTotalPaid = 0;
    let totalTax = 0;

    this.poObject.items.forEach( (item, index ) => {

      subTotalReceived += (item.received * item.price) + ( (item.received * item.price)*(item.tax/100) );
      
      subTotalBilled += (item.billed * item.price) + ( (item.billed * item.price)*(item.tax/100) );
      
      subTotalPaid += (item.paid * item.price) + ( (item.paid * item.price)*(item.tax/100) ); 
      
      totalTax += parseFloat(item.tax_amount);
    });

    itemsTrackData['subTotalReceived'] = subTotalReceived.toFixed(2);
    itemsTrackData['subTotalBilled'] = subTotalBilled.toFixed(2);
    itemsTrackData['subTotalPaid'] = subTotalPaid.toFixed(2);
    itemsTrackData['subTotalTax'] = totalTax.toFixed(2);

    let subTotalOutstanding = parseFloat(this.poObject.gtotal) - parseFloat(itemsTrackData['subTotalPaid']);

    itemsTrackData['subTotalOutstanding'] = ( subTotalOutstanding <= 0 )?0:subTotalOutstanding.toFixed(2);
      
    this.itemsTrackData = itemsTrackData;
  }


  refreshReceipts() {
    this.getPoReceipt(this.purchaseOrderId)
      .then((data) => {

        const items = this._calculatePurchaseOrderItems( data, 'received', 'received' );

        let obj = { items: items };

        obj['poStatus'] = this.getPoStatus(items);

        this._updatePurchaseOrder( obj );
      });
  }

  refreshBills() {
    this.getPoReceipt(this.purchaseOrderId);

      this.getBills(this.purchaseOrderId)
      .then((data) => {
        const items = this._calculatePurchaseOrderItems( data, 'received', 'billed' );

        let obj = { items: items };

        obj['poStatus'] = this.getPoStatus(items);

        this._updatePurchaseOrder(obj);
      });
  }

  refreshPayments() {
    this.getBills(this.purchaseOrderId)
      .then((data) => {
        const items = this.getPaidBillItems(data);

        let obj = { items: items };

        obj['poStatus'] = this.getPoStatus(items);
        
        this._updatePurchaseOrder( obj );
      });

      // get payments...
      this.getPayments(this.purchaseOrderId);
  }

  getPoStatus( items: any ) {

    let po_status = '';

    if (items.every( item => {
          return ( parseInt(item.quantity) - parseInt(item.paid)) <= 0;      
        })){
      return po_status = 'Complete';
    }

    if(items.every( item => {
      return ( parseInt(item.quantity) - parseInt(item.billed)) <= 0;      
    })){
      return po_status = 'Billed'; 
    }

    if(items.every( item => {
      return ( parseInt(item.quantity) - parseInt(item.received)) <= 0;      
    })){
      return po_status = 'Received';
    }else{
      return po_status = 'Processing';
    }
  }

  /*
  * update purchase order items when receipt, bill or payment has been saved/updated...
  */
  saveHandler(object) {  

    this.poTypes[object.type].push(object.data);
        
    if ( object.type == 'receipt'){      
      this.refreshReceipts();
    }

    if (object.type == 'bill'){
      this.refreshBills();
    }

    if ( object.type == 'payment' ){
      this.refreshPayments();
    }

    this.onOpenAccordian();
  }


  /*
  * Update purchase order items status for payment...
  */
  private getPaidBillItems(data: any ){

    const bill_items = [];

    // filter bills based on status..
    // we need only paid bills...
    const bills = data.filter( (bill) => {
      return bill.paid == true;
    });

    bills.forEach( (bill) => {

      bill.items.forEach( (item) => {

        if ( (item.item_id in bill_items ) ){
          bill_items[item.item_id] += item.received;
        }else{
          bill_items[item.item_id] = item.received;
        }
      });
    });


    const po_items = [];

    this.poObject.items.forEach( (item) => {

      const new_item  = item;

      if ( bill_items[item.item_id] ){
        new_item.paid = bill_items[item.item_id];
      }else{
        new_item.paid = 0;
      }

      po_items.push( new_item );

    });

    return po_items;
  }


  private _calculatePurchaseOrderItems(data: any, type: string, po_item_type: string ){

    const type_items = [];

    for ( const r in data ){

      for ( const i of data[r].items ){

        if ( (i.item_id in type_items ) ){
          type_items[i.item_id] +=  i[type];
        }else{
          type_items[i.item_id] = i[type];
        }
      }
    }

    const po_items = [];

    for ( const item of this.poObject.items ){

      const new_item  = item;

      if ( type_items[item.item_id] ){
        new_item[po_item_type] = type_items[item.item_id];
      }else{
        new_item[po_item_type] = 0;
      }

      po_items.push( new_item );
    }

    return po_items;
  }



  /*
  * Update Purchase Order
  */
  private _updatePurchaseOrder( data: any ){

    const objApiData = {};

    objApiData['query'] = { _id: this.poObject._id};

    objApiData['data'] = data;

    this.purchaseOrderService.update( objApiData ).subscribe(
      data => {
        this.getPurchaseOrder(this.purchaseOrderId )
        .then(() => {
          this.poItemsTrack();
          this.onOpenAccordian();
        });
      }
    );
  }


  deletePoType(data){
    this.counts[data.type].splice(data.index, 1);
  }

  onOpenAccordian(){

    this.showSave['receipt'] = true;
    this.showSave['bill'] = true;
    this.showSave['payment'] = true;

    // set add tag..
    if ( (this.poObject.sub_total - this.itemsTrackData['subTotalReceived']) <= 0 ) {
      this.showSave['receipt'] = false;
    }

    const bills = this.poTypes['bill'].filter((data) => {
      return data.paid == false;
    });

    const receipts = this.poTypes['receipt'].filter((data) => {
      return data.billed == false;
    });

    if (bills && !bills.length){
      this.showSave['payment'] = false;
    }

    if (receipts && !receipts.length){
      this.showSave['bill'] = false;
    }

  }

  /*
  * generate PDF of purchase order
  */
  public onPoPDF(){
    const endpoint = '/pdf/po';
    const query = { _id: this.purchaseOrderId };
    const obj = { endpoint: endpoint, query: query };

    this.generatePDF(obj);
  }

  public generatePDF(obj: any){
    this.isLoading = true;
    this.crudService.pdf(obj.endpoint, obj.query).subscribe(
      (data) => {
        window.open(data.filepath, '_blank');
      },
      (error) => {
        this.toastr.error('Something went wrong, please try later', 'Oops!');
      },
      () => {
        this.isLoading = false;
      }
    );
  }



  ////////////////////////////////////////////////////////////
  /////////////////// EMAIL MODAL FUNCTIONS //////////////////
  ////////////////////////////////////////////////////////////
  /*
  * open PO email modal
  */
  public openPOEmailModal(){

    this.emailModel['subject'] = this.poObject.location[0].display + ' Purchase Order #' + this.poObject.po_number + ' – Delivery ' + this.custom_class.formatDate(this.poObject.delivery_date);

    const obj = {
        endpoint: '/purchase-orders/email',
        data: this.emailModel,
        query: {
          _id: this.purchaseOrderId
        }
      };

    this.emailObject = obj;
    this.emailModal.open();
  }

  /*
  * open receipt email modal
  */
  public openEmailModal(obj: any){

    this.emailModel['subject'] = this.poObject.location[0].display + obj.subject;

    obj.data = this.emailModel;
    this.emailObject = obj;
    this.emailModal.open();
  }

   /*
    * called when modal is dismissed...
    */
    emailModalDismissed(frm: NgForm){
      frm.resetForm();
    }

    /*
    * This function is called when modal is closed
    */
    emailModalClosed(){
    }

    /*
    * send email
    */
    sendEmail(frm: NgForm){

      this.isLoading = true;

      this.purchaseOrderService.sendEmail(this.emailObject).subscribe(

        (data) => {
          this.isLoading = false;
          this.toastr.success('Email sent successfully!', 'Success!');
        },
        (error) => {
          this.isLoading = false;
          this.toastr.error('Something went wrong, please try later', 'Oops!');
        },
        () => {
          frm.resetForm();
        }
      );
    }



  /*
  * show toaster message on
  */
  showMessage(msgArray: any) {

    if(msgArray.deleted == 'receipt'){      
      this.refreshReceipts();
    }else if(msgArray.deleted == 'bill'){
      this.refreshBills();
    }else if(msgArray.deleted == 'payment'){
      this.refreshPayments();
    }

    if (msgArray.type == 'success'){
      this.toastr.success(msgArray.message, 'Success!');
    }else{
      this.toastr.error(msgArray.message, 'Oops!');
    }
  }


  /*
  * prepare Purchase Order items for csv export
  */
  preparePoItemsForCSV() {
    
    let items = [];

    items.push({
      name: "name",
      size: "size",
      measure: "measure",
      pack_size: "pack size",
      unit: "unit",
      description: "description",
      quantity: "quantity",
      price: "price",
      tax: "tax(%)",
      tax_amount: "tax amount",
      total: "total",
      received: "received",
      billed: "billed",
      paid: "paid"
    });

    this.poObject.items.forEach( item => {

      let item_unit = item.unit;

      let params = item_unit.split('(');

      let measure_unit = params[1].split(') ');      

      let measure = measure_unit[0];

      let unit = measure_unit[1].trim();

      let size = params[0].trim();

      let pack_size = (params[2])?params[2].substring(0, params[2].length-1):'';

      items.push({
        name: item.name,
        size: size,
        measure: measure,
        pack_size: pack_size,
        unit: unit,
        description: item.description,
        quantity: item.quantity,
        price: item.price,
        tax: item.tax,
        tax_amount: item.tax_amount,
        total: item.total,
        received: item.received,
        billed: item.billed,
        paid: item.paid
      });

    });

    return items;
  }

  /*
  * download po items csv files
  */
  downloadPOItemsCsv() {
    let items = this.preparePoItemsForCSV();
    new Angular5Csv( items, 'PO Items');
  }

  /*
  * export items as csv file
  */
  onPoCSV() {
    this.downloadPOItemsCsv();
  }

}
