import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPurchaseOrderTemplateComponent } from './list-purchase-order-template.component';

describe('ListPurchaseOrderTemplateComponent', () => {
  let component: ListPurchaseOrderTemplateComponent;
  let fixture: ComponentFixture<ListPurchaseOrderTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPurchaseOrderTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPurchaseOrderTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
