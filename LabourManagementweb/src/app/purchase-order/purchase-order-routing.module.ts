import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from '../_guards/index';
import { HomeComponent } from '../home/index';
import { BasicLayoutComponent } from '../components/common/layouts/basicLayout.component';

import { AddPurchaseOrderComponent } from './add-purchase-order/add-purchase-order.component';
import { ListPurchaseOrderComponent } from './list-purchase-order/list-purchase-order.component';
import { ViewPurchaseOrderComponent } from './view-purchase-order/view-purchase-order.component';

import { AddPurchaseOrderTemplateComponent } from './add-purchase-order-template/add-purchase-order-template.component';
import { ListPurchaseOrderTemplateComponent } from './list-purchase-order-template/list-purchase-order-template.component';

const routes: Routes = [
	{
        path: '',
        component: BasicLayoutComponent,
        canActivate: [AuthGuard],
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
            { path: 'dashboard', component: HomeComponent },
            { path: 'purchase-order/new', component: AddPurchaseOrderComponent },
            { path: 'purchase-order/list', component: ListPurchaseOrderComponent},
            { path: 'purchase-order-template/new', component: AddPurchaseOrderTemplateComponent },
            { path: 'purchase-order/edit/:id', component: AddPurchaseOrderComponent },
            { path: 'purchase-order-template/list', component: ListPurchaseOrderTemplateComponent },
            { path: 'purchase-order-template/edit/:id', component: AddPurchaseOrderTemplateComponent },
            { path: 'purchase-order/view/:id', component: ViewPurchaseOrderComponent}
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class PurchaseOrderRoutingModule { }
