import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPurchaseOrderTemplateComponent } from './add-purchase-order-template.component';

describe('AddPurchaseOrderTemplateComponent', () => {
  let component: AddPurchaseOrderTemplateComponent;
  let fixture: ComponentFixture<AddPurchaseOrderTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPurchaseOrderTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPurchaseOrderTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
