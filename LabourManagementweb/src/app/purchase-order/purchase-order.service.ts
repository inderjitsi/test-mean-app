import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/catch';

@Injectable()
export class PurchaseOrderService{
	options: RequestOptions;

	constructor(private http: Http) {

	}


	create(data: any ){

		return this.http.post('/purchase-orders/create', data )
			.map( (res: Response) => res.json() );

	}

	/**
	 * create purchase order template service
	 * @param data
	 */
	create_template(data: any ){
		return this.http.post('/po-templates/create', data )
			.map( (res: Response) => res.json() );
	}

	/**
	 * update purchase order
	 * @param data
	 */
	update_template(data: any){
		return this.http.post('/po-templates/update', data);
	}

	/**
	 * update purchase order
	 * @param data
	 */
	get_template(param: any){

		const params: URLSearchParams = new URLSearchParams();

        for (const key in param) {
            if (param.hasOwnProperty(key)) {
                const val = param[key];
                params.set(key, val);
            }
        }

        this.options = new RequestOptions({ search: params });

        return this.http.get('/po-templates/get', this.options )
        	.map((response: Response) => response.json());

	}

	/**
	 * update purchase order
	 * @param data
	 */
	get(end_point: string, param: any){

		const params: URLSearchParams = new URLSearchParams();

        for (const key in param) {
            if (param.hasOwnProperty(key)) {
                const val = param[key];
                params.set(key, val);
            }
        }

        this.options = new RequestOptions({ search: params });

        return this.http.get(end_point, this.options )
        	.map((response: Response) => response.json());

	}


	/**
	 * update purchase order
	 * @param data
	 */
	update(data: any){
		return this.http.post('/purchase-orders/update', data);
	}

	/**
	 *
	 * @param param
	 */
	getAll( param: any ) {

        this.options = new RequestOptions({ search: param });

        return this.http.get('/purchase-orders', this.options )
        	.map((response: Response) => response.json());
	}

	/**
	 *
	 * @param param
	 */
	getAllTemplates( param: any ) {

        const params: URLSearchParams = new URLSearchParams();

        for (const key in param) {
            if (param.hasOwnProperty(key)) {
                const val = param[key];
                params.set(key, val);
            }
        }

        this.options = new RequestOptions({ search: params });

        return this.http.get('/po-templates', this.options ).map((response: Response) => response.json());
    }
    getPurchaseOrderById(id){
    	return this.http.get('/purchase-orders/get', {params: {_id: id}}).map((response: Response) => response.json());
    }
    savePoReceipt(data){
    	return this.http.post('/po-receipt' , data).map((response: Response) => response.json());
    }
    getPoReceipt(poId){
    	return this.http.get('/po-receipt/' + poId).map((response: Response) => response.json());
    }

    updatePoReceipt(data){
    	return this.http.put('/po-receipt', data).map((response: Response) => response.json());
    }

    deleteReceipt(param: any) {

        this.options = new RequestOptions({ search: param });

        return this.http.delete('/po-receipt', this.options )
            .map((response: Response) => response.json());
    }


    savePoBill(data){
    	return this.http.post('/po-bill' , data).map((response: Response) => response.json());
    }
    getPoBill(poId){
    	return this.http.get('/po-bill/' + poId).map((response: Response) => response.json());
    }
    updatePoBill(data){
    	return this.http.put('/po-bill', data).map((response: Response) => response.json());
    }

    deleteBill(param: any) {

        this.options = new RequestOptions({ search: param });

        return this.http.delete('/po-bill', this.options )
            .map((response: Response) => response.json());
    }



    savePoPayment(data){
        return this.http.post('/po-payment' , data).map((response: Response) => response.json());
    }
    getPoPayment(poId){
        return this.http.get('/po-payment/' + poId).map((response: Response) => response.json());
    }
    updatePoPayment(data){
        return this.http.put('/po-payment', data).map((response: Response) => response.json());
    }

    deletePayment(param: any) {

        this.options = new RequestOptions({ search: param });

        return this.http.delete('/po-payment', this.options )
            .map((response: Response) => response.json());
    }


    /*
    * send purchase order email
    */
    sendEmail(obj: any){
        return this.http.post( obj.endpoint, obj );
    }





}
