import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search-purchase-order',
  templateUrl: './search-purchase-order.component.html',
  styleUrls: ['./search-purchase-order.component.css']
})
export class SearchPurchaseOrderComponent implements OnInit {

  @Input() searchPlaceHolder: String = '';
  @Output() onSearchEvent = new EventEmitter < any > ();

  constructor() {
  }

  ngOnInit() {
  }

  onSearch(search: any){
    this.onSearchEvent.emit(search);
  }

}
