import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseOrderBillComponent } from './purchase-order-bill.component';

describe('PurchaseOrderBillComponent', () => {
  let component: PurchaseOrderBillComponent;
  let fixture: ComponentFixture<PurchaseOrderBillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseOrderBillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseOrderBillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
