import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { DatepickerOptions } from 'ng2-datepicker';
import * as frLocale from 'date-fns/locale/fr';
import {PurchaseOrderService} from '../purchase-order.service';
import { ActivatedRoute } from '@angular/router';
import {appConfig} from '../../app.config';
import { Custom } from '../../classes/custom';
import { SweetAlertService } from '../../_services/sweetalert.service';

@Component({
  selector: 'app-purchase-order-bill',
  templateUrl: './purchase-order-bill.component.html',
  styleUrls: ['./purchase-order-bill.component.css']
})
export class PurchaseOrderBillComponent implements OnInit {

  @Input() poReceipt;
  @Input() poObject;

  @Input() bill: any = {};
  @Input() index;
  file: any = {};
  readOnly = true;
  @Output() onSave = new EventEmitter();
  @Output() delete = new EventEmitter();
  @Output() pdfEvent = new EventEmitter< any >();
  @Output() showMessage = new EventEmitter <any>();
  @Output() emailEvent = new EventEmitter <any>();

  public poReceiptCopy: any = [];

  public custom_class = new Custom();

  apiUrl = appConfig.apiUrl;

 options: DatepickerOptions = {
    minYear: 1970,
    maxYear: 2018,
    displayFormat: 'MMM D[,] YYYY',
    barTitleFormat: 'MMMM YYYY',
    firstCalendarDay: 0, // 0 - Sunday, 1 - Monday
    locale: frLocale,
    minDate: new Date(Date.now()), // Minimal selectable date
    maxDate: new Date(Date.now())  // Maximal selectable date
  };

  constructor(
    private purchaseOrderService: PurchaseOrderService,
    private alertService: SweetAlertService,
    private route: ActivatedRoute ) {
  }

  ngOnInit() {
    this.setDefaultValues();

    this.poReceiptCopy = this.createReceiptInputs();

    this.setForTagsInput(this.poReceiptCopy);
  }

  ngOnChanges(){    
  }


  setForTagsInput(array){
      array.map((data) => {
        data.display = data.receipt_number + '';
        data.value = data._id;
      });

      if ( array.length > 1 ){

      let found = array.findIndex( element => {
              return (element.id !== 'all')
            });
      console.log( "found", found );

      if( !found ){
        array.unshift({name: 'All', 'id': 'all', 'display': 'All', value: 'all'});
      }    
    }
  }

  /*
  * create Receipt Tag Inputs
  */
  createReceiptInputs() {

    let arrayReceipts = [];

    this.poReceipt.forEach( item => {

      if ('_id' in this.bill ) {

        if( 'po_receipt' in this.bill ){

          this.bill.po_receipt.forEach( receipt => {

            if (item.billed == false || receipt._id == item._id  ){

              arrayReceipts.push(item);              
            }
          });
        }else{
          if (item.billed == false){
            arrayReceipts.push(item);            
          }
        }
      }else{
        if (item.billed == false){
          arrayReceipts.push(item);            
        }
      }

    });    

    return arrayReceipts;
  }


  onFocusing() {
  }

  setDefaultValues() {
    if (!this.bill){
      this.bill = {};
    }
    if (!this.bill._id){
      this.readOnly = false;
    }
    this.bill.bill_date = this.bill.bill_date ? this.bill.bill_date : new Date();
    this.bill.items = this.bill.items && this.bill.items.length ? this.bill.items : [];
    this.bill.sub_total = this.bill.sub_total ? this.bill.sub_total : 0;
    this.bill.tax = this.bill.tax ? this.bill.tax : 0;    
    this.bill.total = this.bill.total ? this.bill.total : 0;

  }
  fileChange(event){
    this.file = event.target.files && event.target.files ? event.target.files[0] : null;
  }


  /*
  * Save new bill into the database...
  */
  saveBill(){
    
    const formData = new FormData();
    
    if (this.file){
        formData.append('files', this.file);
    }

    this.bill.po_id = this.poObject._id ;
    
    formData.append( 'payload', JSON.stringify( this.bill ) );
    
    this.purchaseOrderService.savePoBill(formData).subscribe((data) => {

      const msgReturn = {'message': 'Bill has been created successfully!', 'type': 'success' };
      this.showMessage.emit(msgReturn);

      this.onSave.emit({data: data, type: 'bill'});
      this.cancel();
    }, (error) => {
      const msgReturn = {'message': 'Something went wrong, please try later!', 'type': 'error' };
      this.showMessage.emit(msgReturn);
    });
  }
  
  cancel(){
    this.delete.emit({index: this.index, type: 'bill'});
  }

  editBill(){

    const formData = new FormData();

    if (this.file){
        formData.append('files', this.file);
    }
    
    this.bill.po_id = this.poObject._id ;
    
    formData.append('payload', JSON.stringify(this.bill));

    console.log("this.bill", this.bill );
    
    this.purchaseOrderService.updatePoBill(formData).subscribe(
      (data) => {
       const msgReturn = {'message': 'Bill has been updated successfully!', 'type': 'success' };
       this.showMessage.emit(msgReturn);
       this.onSave.emit({data: data, type: 'bill'});
    }, (error) => {
      const msgReturn = {'message': 'Something went wrong, please try later!', 'type': 'error' };
      this.showMessage.emit(msgReturn);
    });
    
  }

  /*filterReceipts(){
    this.poReceipt = this.poReceipt.filter((data)=>{
      return data.billed == false;
    })
  }*/

  onAdding(item: any ){
    this.addItemsToBill(item);
  }

  onRemoving(item: any ){
    this.addItemsToBill(item);
  }

  private addItemsToBill(item: any) {

    if(item.id == 'all'){

      this.bill.items = [];

      this.bill.po_receipt = [];

      const receipts = this.poReceiptCopy.filter( data => {
         return data.value != 'all';
      });

      this.bill.po_receipt = receipts;

      this.bill.po_receipt.map( receipt => {

        receipt.items.map((data) => {
          this.bill.items.push(data);
        });
      });

    }else{
      this.bill.items = [];

      this.bill.po_receipt.map( receipt => {

        receipt.items.map((data) => {
          this.bill.items.push(data);
        });
      }); 
    }        

    this.calculateTotal();
  }


  calculateTotal() {

    let sub_total = 0;
    let total_tax = 0;

    this.bill.items.forEach( (item, index) => {

      const tax_percentage = (parseFloat(item.tax))?parseFloat(item.tax):0;

      const quantity = parseInt(item.received );
      
      const price = parseFloat(item.price);
    
      const total = quantity * price ? quantity * price : 0;

      const item_tax_amount = total * (tax_percentage/100);

      total_tax += item_tax_amount;

      sub_total += total;

    });

    this.bill.tax = parseFloat( <any>total_tax );

    this.bill.sub_total = sub_total;
    
    let grand_total = sub_total + total_tax ;

    this.bill.total = parseFloat( <any>grand_total );
  }

  onPdf() {
    const endpoint = '/pdf/bill';
    const query = {_id: this.bill._id };
    const obj = { endpoint: endpoint, query: query };
    this.pdfEvent.emit(obj);
  }


  /*
  *  open email modal in the parent component...
  */
  onEmail() {

    const obj = {
      endpoint: '/po-bill/email',
      query: {
        _id: this.bill._id
      },
      subject: ' Bill #' + this.bill.bill_number + ' – Bill Date ' + this.custom_class.formatDate(this.bill.bill_date)
    };
    this.emailEvent.emit( obj );
  }

  /*
  * Delete Receipt
  */
  onDelete() {

    this.alertService.confirm({
      title: 'Are you sure?',
      text: 'This action can\'t be undone',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      // Yes, delete it
      if (result.value) {
        this.deleteBill();        
      }
      // No, keep it
      if (result.dismiss === "cancel") {        
      }
    });
  }

  /*
  * delete receipt
  */
  deleteBill() {
    
    let query = {
      _id: this.bill._id
    };

    this.purchaseOrderService.deleteBill(query).subscribe(
      data => {
        const msgReturn = {'message': 'Bill has been deleted successfully!', 'type': 'success', 'deleted':'bill' };
        this.showMessage.emit(msgReturn);
      },
      error => {        
        const msgReturn = {'message': 'Something went wrong, please try later!', 'type': 'error' };
        this.showMessage.emit(msgReturn);
      }
    );
  }


}
