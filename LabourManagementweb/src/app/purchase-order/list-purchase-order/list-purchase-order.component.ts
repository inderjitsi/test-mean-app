import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { PaginationInstance } from 'ngx-pagination';
import { ToastsManager, Toast } from 'ng2-toastr/ng2-toastr';

import { PurchaseOrderService } from '../purchase-order.service';

@Component({
  selector: 'app-list-purchase-order',
  templateUrl: './list-purchase-order.component.html',
  styleUrls: ['./list-purchase-order.component.css']
})
export class ListPurchaseOrderComponent implements OnInit {

  public isLoading = true;
  public purchase_orders: any = [];
  public searchPlaceHolder: String = 'Search';

  public filterButton = 'all';
  public showCheckBoxesDependentButtons = false;
  public checkAllObject = {};
  public btnCheckAll = false;
  public filter_keyword = 'Pending';
  public filter_query: any = {poStatus: {$ne: 'Complete'}};

  // pagination & search fileters
  public sort = 'po_number';
  public sortBy = 'asc';
  public search = '';
  public sortColumn = 'po_number';
  public isDeleted: any = 'all' ;
  public maxSize = 10;
  public directionLinks = true;
  public config: PaginationInstance = {
      id: 'server',
      itemsPerPage: 10,
      currentPage: 1,
      totalItems: 0
  };
  public labels: any = {
      previousLabel: 'Previous',
      nextLabel: 'Next',
      screenReaderPaginationLabel: 'Pagination',
      screenReaderPageLabel: 'page',
      screenReaderCurrentLabel: `You're on page`
  };
  public sorting = 'sorting_desc';

  constructor(
    private poService: PurchaseOrderService,
    public toastr: ToastsManager,
    public vcr: ViewContainerRef ) {
     this.toastr.setRootViewContainerRef(vcr);
   }


  ngOnInit() {
    this.getAll();
  }

  /*
  * get all purchase orders
  */
  private getAll() {

    this.isLoading = true;
    const configData = {
        currentPage: this.config.currentPage,
        itemsPerPage: this.config.itemsPerPage,
        sort: this.sort,
        sortBy: this.sortBy,
        search: this.search,
        isDeleted: this.isDeleted,
        query: this.filter_query
    };

    this.poService.getAll( configData ).subscribe(
        data => {
            this.isLoading = false;
            this.checkAll(false);
            this.purchase_orders = (data.res) ? data.res : [];
            this.config.totalItems = (data.totalRecords) ? data.totalRecords : 0;
        },
        error => {
            this.isLoading = false;
        }
    );
  }

  /**
   * pagination events::
   */
  onPageChange(number: number) {
    this.config.currentPage = number;
    this.getAll();
  }

  onSearch(search: any) {

      this.search = search;

      this.getAll();
  }

  onSortChange(sort) {

      this.sortColumn = sort;

      if (this.sortBy == 'asc'){
          this.sortBy = 'desc';

          this.sort = '-' + sort;

          this.sorting = 'sorting_desc';
      } else {

          this.sort = sort;

          this.sortBy = 'asc';
          this.sorting = 'sorting_asc';
      }

      this.getAll();
  }


  /*
    * filter records
    */
    filterRecords(data: any){

      this.isDeleted = data;

      this._setFilterButtonsStatus(data);

      this.getAll();
  }

  /*
  * set active class to the appropriate filter buttons (active, deactive, all)
  */
  private _setFilterButtonsStatus(data: any ){

      if (data == 'all'){
          this.filterButton = 'all';
      }else if (data == true){
          this.filterButton = 'deactive';
      }else if (data == false){
          this.filterButton = 'active';
      }
  }


  /////********* CHECKBOXES RELATED FUNCTIONS STARTS HERE *******************////////////

  /*
  * check all checkboxes when checkAll button is clicked
  */
  checkAll(status: boolean) {

      this.btnCheckAll = status;

      this._setCheckBoxesStatus( status );

      // show/hide buttons dependent on checkboxes...
      this._showCheckBoxesDependentButtons();
  }

  /*
  * check if all the checkboxes are checked? then check the checkAll button as well
  */
  isAllChecked() {

      if (!this.checkAllObject) return;

      let checkFlag = true;

      for (const i in this.checkAllObject  ){

          if ( !this.checkAllObject[i] ){
              checkFlag = false;
              break;
          }
      }
      return checkFlag;
  }


  /*
  * function will be called when any checkbox is clicked...
  */
  onCheck(id: string, status: boolean, index ){

      this.checkAllObject[id] = status;

      // set status of check all button
      this.btnCheckAll = this.isAllChecked();

      // show/hide buttons dependent on checkboxes...
      this._showCheckBoxesDependentButtons();
  }

  /*
  * set bit button visibility
  */
  private _showCheckBoxesDependentButtons() {

      let flag = false;

      for (const i in this.checkAllObject  ){

          if ( this.checkAllObject[i] ){
              flag = true;
              break;
          }
      }
      this.showCheckBoxesDependentButtons = flag;
  }

  /*
  * set checkboxes status
  */
  private _setCheckBoxesStatus(status: boolean) {
      if (!this.purchase_orders ) return;
      for (const i in this.purchase_orders  ){
          this.checkAllObject[ this.purchase_orders[i]._id ] = status;
      }
  }
  /////********* CHECKBOXES RELATED FUNCTIONS ENDS HERE *******************////////////

  /* change vendor status on toggle switch */
  setStatus(isDeleted: Boolean, id: any ){
      this.poService.update( { query: {_id: id}, data: {isDeleted: isDeleted} } ).subscribe(
          data => {
              this.getAll();
          },
          error => {
              this.toastr.error('Error occured! please try later', 'Oops!');
          }
      );
  }

  
  filterOrders(filter_keyword: string ) {
    
    this.filter_keyword = filter_keyword;

    if(filter_keyword == 'Pending'){       
      this.filter_query = { poStatus: {$ne: 'Complete'}};
    }else if(filter_keyword == 'Complete'){
      this.filter_query = { poStatus: 'Complete' };
    }

    this.getAll();    
  }


}
