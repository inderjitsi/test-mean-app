import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgDatepickerModule } from 'ng2-datepicker';
import { BsModalModule } from 'ng2-bs3-modal';
import { DropdownModule } from 'ng2-dropdown';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { TagInputModule } from 'ngx-chips';
import { AccordionModule } from 'ng2-accordion';
import { NgSelectModule } from '@ng-select/ng-select';
import { TooltipModule } from "ngx-tooltip";

import { SharedModule } from '../_shared/shared.module';
import { PurchaseOrderService } from './purchase-order.service'
import { AddPurchaseOrderComponent } from './add-purchase-order/add-purchase-order.component';
import { PurchaseOrderRoutingModule } from './purchase-order-routing.module';
import { ListPurchaseOrderComponent } from './list-purchase-order/list-purchase-order.component';
import { VendorService } from '../_services/vendor.service';
import { LocationService } from '../_services/location.service';
import { QuotePurchaseOrderComponent } from './quote-purchase-order/quote-purchase-order.component';
import { PoQuoteService } from './../_services/poquote.service';
import {validationModule} from '../_directives/validations/validation.module';
import { SearchPurchaseOrderComponent } from './search-purchase-order/search-purchase-order.component';
import { AddPurchaseOrderTemplateComponent } from './add-purchase-order-template/add-purchase-order-template.component';
import { ListPurchaseOrderTemplateComponent } from './list-purchase-order-template/list-purchase-order-template.component';
import { ViewPurchaseOrderComponent } from './view-purchase-order/view-purchase-order.component';
import { PurchaseOrderReceiptComponent } from './purchase-order-receipt/purchase-order-receipt.component';
import { PurchaseOrderBillComponent } from './purchase-order-bill/purchase-order-bill.component';
import { PurchaseOrderPaymentComponent } from './purchase-order-payment/purchase-order-payment.component';
import { CrudService } from './../_services/crud.service';
import { SweetAlertService } from '../_services/sweetalert.service';

@NgModule({
  imports: [
    CommonModule,
    BsModalModule,
    PurchaseOrderRoutingModule,
    ReactiveFormsModule,
    NgDatepickerModule,
    DropdownModule,
    SharedModule,
    TagInputModule,
    validationModule,
    NgxPaginationModule,
    AccordionModule,
    NgSelectModule,
    TooltipModule
  ],
  declarations: [
    AddPurchaseOrderComponent,
    ListPurchaseOrderComponent,
    QuotePurchaseOrderComponent,
    SearchPurchaseOrderComponent,
    AddPurchaseOrderTemplateComponent,
    ListPurchaseOrderTemplateComponent,
    ViewPurchaseOrderComponent,
    PurchaseOrderReceiptComponent,
    PurchaseOrderBillComponent,
    PurchaseOrderPaymentComponent
  ],
  providers: [
    PurchaseOrderService,
    VendorService,
    LocationService,
    PoQuoteService,
    CrudService,
    SweetAlertService
  ]
})

export class PurchaseOrderModule { }
