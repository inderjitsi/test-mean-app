import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { DatepickerOptions } from 'ng2-datepicker';
import * as frLocale from 'date-fns/locale/fr';
import { PurchaseOrderService } from '../purchase-order.service';
import { ActivatedRoute } from '@angular/router';
import { appConfig } from '../../app.config';
import { Custom } from '../../classes/custom';
import { SweetAlertService } from '../../_services/sweetalert.service';

@Component({
  selector: 'app-purchase-order-receipt',
  templateUrl: './purchase-order-receipt.component.html',
  styleUrls: ['./purchase-order-receipt.component.css']
})
export class PurchaseOrderReceiptComponent implements OnInit {

  @Input() poObject;
  @Input() poReceipt: any = {};
  poObjectData: any = [];
  @Input() index;
  file: any = {};
  @Output() onSave = new EventEmitter();
  @Output() delete = new EventEmitter();
  @Output() pdfEvent = new EventEmitter< any >();
  @Output() showMessage = new EventEmitter <any>();
  @Output() emailEvent = new EventEmitter <any>();

  public custom_class = new Custom();

  public poReceiptCopy: any;

  readOnly = true;
  apiUrl = appConfig.apiUrl;

  public receiptItemsTag: any = [];

  options: DatepickerOptions = {
    minYear: 1970,
    maxYear: 2018,
    displayFormat: 'MMM D[,] YYYY',
    barTitleFormat: 'MMMM YYYY',
    firstCalendarDay: 0, // 0 - Sunday, 1 - Monday
    locale: frLocale,
    minDate: new Date(Date.now()), // Minimal selectable date
    maxDate: new Date(Date.now())  // Maximal selectable date
  };

  constructor(
    private purchaseOrderService: PurchaseOrderService,
    private alertService: SweetAlertService,
    private route: ActivatedRoute ) {
  }

  ngOnInit() {

    this.poReceiptCopy = JSON.parse(JSON.stringify(this.poReceipt));

    this.poObjectData = JSON.parse(JSON.stringify(this.poObject)) ;

    this.createReceiptItemsTag();

    this.setDefaultValues();
  }

  private createReceiptItemsTag(){

    const arr = [];

    for (const itemObj of this.poObject.items ){

      const receiptItem = this.getReceiptItem(itemObj.item_id);
      const receiptItemReceived = (Object.keys(receiptItem).length > 0 ) ? receiptItem[0].received : 0;

      const received = ( parseInt(itemObj.quantity) - (  parseInt(itemObj.received) - parseInt(receiptItemReceived)  ) );

      if ( received > 0 ){

        const item = {
          'display': itemObj.name,
          'value': itemObj.item_id + '',
          'id' : itemObj.id,
          'item_id' : itemObj.item_id,
          'name' : itemObj.name,
          'description' : itemObj.description,
          'unit' : itemObj.unit,
          'received': received,
          'price' : itemObj.price,
          'tax':itemObj.tax
        }

        arr.push(item);
      }
    }

    if (arr.length > 1){
      if (this.findIndex( arr, 'id', 'all') == -1){
        arr.unshift({name: 'All', 'id': 'all', 'display': 'All', value: 'all'});
      }
    }

    this.receiptItemsTag = arr;

    console.log('this.receiptItemsTag', this.receiptItemsTag);

  }


  getReceiptItem(item_id: any ){

    if ( '_id' in this.poReceiptCopy ) {
        return this.poReceiptCopy.items.filter(function(item){

          if (item.item_id == item_id ){
            return item;
          }else{
            return false;
          }
        });
    }else{
      return false;
    }
  }

  getPOItem(item_id: any){
    if ( Object.keys(this.poObject.items).length > 0 ) {
        return this.poObject.items.filter(function(item){

          if (item.item_id == item_id ){
            return item;
          }else{
            false;
          }
        });
    }else{
      return false;
    }
  }

  private getItemPendingQty(item_id: any ){

    const POitem = this.getPOItem(item_id);

    const receiptItem = this.getReceiptItem(item_id);

    const receiptItemQty = (Object.keys(receiptItem).length > 0 ) ? receiptItem[0].received : 0;

    const qty = ( parseInt(POitem[0].quantity) - (  parseInt(POitem[0].received) - parseInt(receiptItemQty) ) );

    return qty;
  }



  setForTagsInput(array){
      array.map((data) => {
        data.display = data.name;
        data.value = data.id;
      })
  }
  setDefaultValues(){
    if (!this.poReceipt){
      this.poReceipt = {};
    }
    if (!this.poReceipt._id){
      this.readOnly = false;
    }
    this.poReceipt.receipt_date = this.poReceipt.receipt_date ? this.poReceipt.receipt_date : new Date();
  }

  fileChange(event){
    this.file = event.target.files && event.target.files ? event.target.files[0] : null;
  }

  saveReceipt(){

    const formData = new FormData();

    if (this.file){
      formData.append('files', this.file);
    }

    this.poReceipt.po_id = this.poObject._id;

    formData.append('payload', JSON.stringify(this.poReceipt));

    this.purchaseOrderService.savePoReceipt(formData).subscribe( (data) => {

      const msgReturn = {'message': 'Receipt has been created successfully!', 'type': 'success' };
      this.showMessage.emit(msgReturn);

      this.onSave.emit({data: data, type: 'receipt'});
      this.cancel();

    }, (error) => {
      const msgReturn = {'message': 'Something went wrong, please try later!', 'type': 'error' };
      this.showMessage.emit(msgReturn);
    })
  }

  cancel(){
    this.delete.emit({index: this.index, type: 'receipt'});
  }

  editReceipt(){

    const formData = new FormData();

    if (this.file){
      formData.append('files', this.file);
    }

    this.poReceipt.po_id = this.poObject._id;

    formData.append('payload', JSON.stringify(this.poReceipt));

    this.purchaseOrderService.updatePoReceipt(formData).subscribe((data) => {

      const msgReturn = {'message': 'Receipt has been updated successfully!', 'type': 'success' };
      this.showMessage.emit(msgReturn);
      this.onSave.emit({ data: data, type: 'receipt'});
    }, (error) => {

      const msgReturn = {'message': 'Something went wrong, please try later!', 'type': 'error' };
      this.showMessage.emit(msgReturn);
    })
  }

  addReceived(index, item_id: any){

    const item_qty = this.getItemPendingQty(item_id );

    if (!this.poReceipt.items[index].received ){
      this.poReceipt.items[index].received  = 1;
    }

    if ( this.poReceipt.items[index].received >= item_qty ){
      //return false;
    }

    this.poReceipt.items[index].received = this.poReceipt.items[index].received + 1;
  }

  removeReceived(index){    
    if ( !this.poReceipt.items[index].received || (this.poReceipt.items[index].received == 1 ) ){
      this.poReceipt.items[index].received  = 1;
    }else{
      this.poReceipt.items[index].received = this.poReceipt.items[index].received - 1;
    }
  }

  findIndex(array, key, value){
   const index = array.findIndex((data) => {
      return data[key] == value;
    })
   return index;
  }

  onItemAdding(tagsItems){
    if (this.findIndex(tagsItems, 'value', 'all') > -1){
      this.poReceipt.items = [];
      const items = this.receiptItemsTag.filter( (data) => {
         return data.value != 'all';
      });
      this.poReceipt.items = items;
    }
  }

  onPdf(){
    const endpoint = '/pdf/receipt';
    const query = {_id: this.poReceipt._id };
    const obj = { endpoint: endpoint, query: query };

    this.pdfEvent.emit(obj);
  }

  onEmail(){

    const obj = {
        endpoint: '/po-receipt/email',
        query: {
          _id: this.poReceipt._id
        },
        subject: ' Receipt #' + this.poReceipt.receipt_number + ' – Receipt Date ' + this.custom_class.formatDate(this.poReceipt.receipt_date)
    };

    this.emailEvent.emit( obj );
  }

  /*
  * Delete Receipt
  */
  onDelete() {

    this.alertService.confirm({
      title: 'Are you sure?',
      text: 'This action can\'t be undone',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      // Yes, delete it
      if (result.value) {
        this.deleteReceipt();        
      }
      // No, keep it
      if (result.dismiss === "cancel") {        
      }
    });
  }


  /*
  * delete receipt
  */
  deleteReceipt() {
    
    let query = {
      _id: this.poReceipt._id
    };

    this.purchaseOrderService.deleteReceipt(query).subscribe(
      data => {        
        const msgReturn = {'message': 'Receipt has been deleted successfully!', 'type': 'success', 'deleted':'receipt' };
        this.showMessage.emit(msgReturn);
      },
      error => {        
        const msgReturn = {'message': 'Something went wrong, please try later!', 'type': 'error' };
        this.showMessage.emit(msgReturn);
      }
    );
  }



}
