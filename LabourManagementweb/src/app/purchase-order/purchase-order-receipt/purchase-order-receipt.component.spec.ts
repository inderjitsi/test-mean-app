import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseOrderReceiptComponent } from './purchase-order-receipt.component';

describe('PurchaseOrderReceiptComponent', () => {
  let component: PurchaseOrderReceiptComponent;
  let fixture: ComponentFixture<PurchaseOrderReceiptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseOrderReceiptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseOrderReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
