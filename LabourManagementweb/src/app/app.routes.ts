import {Routes} from '@angular/router';

import { AuthGuard } from './_guards/index';



export const ROUTES: Routes = [
    {
        path: '',
        component: BasicLayoutComponent,
        data: { title: 'Home' },
        canActivate: [AuthGuard],
    },
    {
      path: '', component: BlankLayoutComponent,
      children: [
      ]
    }

    //{ path: '**', redirectTo: 'dashboard' }
];
