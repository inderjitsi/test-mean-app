import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response, URLSearchParams } from '@angular/http';

@Injectable()
export class PoQuoteService {

    options: RequestOptions;

	constructor( private http: Http ) {
	}

	/*
    * send po to quote
    */
    /*sendItemsToQuote(data: any){

        return this.http.post('/quotes/create', data );
    }*/

    /*
    * find one record
    */
    findOne(param: any){

        const params: URLSearchParams = new URLSearchParams();

        for (const key in param) {
            if (param.hasOwnProperty(key)) {
                const val = param[key];
                params.set(key, val);
            }
        }

        this.options = new RequestOptions({ search: params });

        return this.http.get('/purchase-order-quotes/findOne', this.options )
            .map( (response: Response ) => response.json() );
    }

    /*
    * get quote data by url key
    */
    delete( param: any ){

        const params: URLSearchParams = new URLSearchParams();

        for (const key in param) {
            if (param.hasOwnProperty(key)) {
                const val = param[key];
                params.set(key, val);
            }
        }

        this.options = new RequestOptions({ search: params });

        return this.http.delete('/purchase-order-quotes/delete', this.options );
    }


}
