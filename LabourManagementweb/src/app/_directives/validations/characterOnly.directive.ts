import {Directive, ElementRef, HostListener, Input, OnChanges} from '@angular/core';

@Directive({
    selector: '[charactersOnly]'
})
export class CharactersOnlyDirective implements OnChanges {
    @Input()charactersOnly: any;
    @Input() spaceAllowed: any;
    @Input() dashAllowed: any;
    constructor(private el: ElementRef) {}
    @HostListener('keydown', ['$event'])
    keyDownEvent(event: KeyboardEvent) {
        let keyCondition = (event.which < 65 || event.which > 90);
        if (this.spaceAllowed){
            keyCondition = ((event.which < 65 ||  event.which > 90) && event.which != 32);
        }
        if (this.dashAllowed){
             keyCondition = ((event.which < 65 ||  event.which > 90) && event.which != 32  && event.which != 173);
        }
        if (event.key.length === 1 && keyCondition) {
            event.preventDefault();
        }
        if (this.spaceAllowed){}
    }
    ngOnChanges(changes) {

    }
}
