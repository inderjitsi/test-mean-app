import { NgModule, ModuleWithProviders }      from '@angular/core';
import { FormsModule, ReactiveFormsModule }          from '@angular/forms';
import {NumbersOnlyDirective} from './numbersOnly.directive';
import {CharactersOnlyDirective} from './characterOnly.directive'

@NgModule({
  imports:      [FormsModule, ReactiveFormsModule],
  declarations: [NumbersOnlyDirective, CharactersOnlyDirective],
  exports: [NumbersOnlyDirective, CharactersOnlyDirective],
  providers: []
})
export class validationModule {
  static forRoot(): ModuleWithProviders { return {ngModule: validationModule, providers: []}; }
}
