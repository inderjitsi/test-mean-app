import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';

import {ROUTES} from './app.routes';
import { AppComponent } from './app.component';

// App views
import {DashboardsModule} from './views/dashboards/dashboards.module';
import {AppviewsModule} from './views/appviews/appviews.module';

// App modules/components
import {LayoutsModule} from './components/common/layouts/layouts.module';



// Core Modules..
import { customHttpProvider } from './_helpers/index';
import { AuthGuard } from './_guards/index';
import { AuthenticationService, UserService } from './_services/index';
import {ToastModule} from 'ng2-toastr/ng2-toastr';
import { BsModalModule } from 'ng2-bs3-modal';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppBootstrapModule } from './app-bootstrap/app-bootstrap.module';
import {SharedModule} from './_shared/shared.module';
import { CustomOption } from './_shared/toastr-custom-option';
import {ToastOptions} from 'ng2-toastr';

import { LabourManagementModule } from './labour_management/labour-management.module';




@NgModule({
  declarations: [
    AppComponent    
    ],
  imports: [
    BrowserModule,
    FormsModule,
    BsModalModule,
    AppBootstrapModule,
    BrowserAnimationsModule,
    ToastModule.forRoot(),
    HttpModule,
    LayoutsModule       
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy},
    customHttpProvider,
    AuthGuard,
    AuthenticationService,   
    {provide: ToastOptions, useClass: CustomOption}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
