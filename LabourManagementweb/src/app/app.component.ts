import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { LoaderService } from './_services/loader.service';

declare var jQuery: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  isLoading: boolean = false;
  
  constructor(private loaderService: LoaderService,
    private router: Router) {
  }
  
  ngOnInit() {

  }
}
