import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Subscription } from "rxjs/Subscription";

import { SecurityService } from '../_services/security.service';
import { LocalDataService } from '../_services/local-data.service';

import { UserService } from '../_services/user.service';

// admin access levels
import { ADMIN_ACCESS } from '../_shared/admin.access';

@Injectable()
export class AuthGuard implements CanActivate {

    private subscription: Subscription;

    private permissions: any;

    constructor(
        private dataService: LocalDataService,
        private userService: UserService,        
        private securityService: SecurityService,
        private router: Router) { 

        this.dataService.currentPermissions.subscribe( permissions => {        
            this.permissions = permissions;
        });
        
        /**
         * check if current user is logged in, 
         * if yes, get the permissions and set it in subject.
         */
        if ( localStorage.getItem('currentUser') ) {
            this.checkUserAuthenticity();    
        }        
    }

    /*
    * check user authenticity type
    */
    checkUserAuthenticity() {  

        this.userService.getCurrent().subscribe(
            user => {

                let role_id = user.role_id; 
                let user_type = user.user_type;  

                this.dataService.current_user.next(user);

                if( user_type == 'admin' ) {

                    let permissions = JSON.parse(JSON.stringify( ADMIN_ACCESS ) );
        
                    this.dataService.setPermissions(permissions);
        
                } else if( user_type == 'employee' ) {        
                    
                    this.getPermissions( role_id ).then( permissions => {
                        this.dataService.setPermissions(permissions['permissions']);                            
                    }).catch(error => {
                        console.error("oops error occured", error );
                    });
                }
            },
            error => {
                localStorage.removeItem('currentUser');
                window.location.href = '/login';
                console.error('error', error);
            }
        ); 
    }

    /*
    * get the role based permissions access for different pages & menu items...
    */    
    getPermissions( role_id: string ) {

        return new Promise((resolve, reject) => {

            const queryObject = { 
                query: {
                    role_id: role_id
                },
                columns: { 
                    permissions: 1 
                }
            };

            this.securityService.get(queryObject).subscribe(

                data => {
                    resolve(data);                    
                },
                error => {
                    reject(error);
                }
            );
        });
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        
        if ( localStorage.getItem('currentUser') ) {

            let visited_rout =  state.url.substr(1);

            let permission = this.permissions[visited_rout];            

            if( !this.router.navigated ) { // browser refreshed mannually
                setTimeout( () => {
                    let permission_off = this.permissions[visited_rout];
                    if ( typeof permission_off === 'undefined' || permission_off ) {
                        return true;
                    } else if( !permission_off ) {
                        localStorage.removeItem('currentUser');
                        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
                        return false;
                    }
                }, 1000 );                
            } else {
                if ( typeof permission === 'undefined' || permission ) {
                    return true;
                } else if ( !permission ) {
                    return false;
                }
            }
        }
        // not logged in so redirect to login page with the return url
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
        return false;
    }    

    ngOnDestroy() {
      this.subscription.unsubscribe();
    }
}
