export class PO {
	po_number = '';
	po_date = Date;
	location = '';
	locationObject = '';
	vendor = '';
	vendorObject = '';
	address = '';
	items: POItem[];
	sub_total = '';
	tax = '';
	gtotal = '';
	notes = '';
	sent_to_vendor = false;
}

export class Address {
  street = '';
  city   = '';
  state  = '';
  zip    = '';
  country = '';
}

export class POItem {
	id = '';
	item_id = '';
	name = '';
	description   = '';
	unit =       '';
	quantity  = '';
	price    = '';
	tax = '';
	tax_amount = '';
	total = '';
	received = 0;
	billed = 0;
	paid = 0;
}





